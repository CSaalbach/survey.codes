<?php
if ($_SERVER['HTTP_HOST'] != "survey.codes") {
        header("Location: https://survey.codes/");
}
?>

<!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="utf-8"/>
	<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
	<meta content="width=device-width, initial-scale=1" name="viewport"/>

	<title>404</title>
	
	<link href="https://survey.codes/favicon.ico" rel="shortcut icon" type="image/x-icon"/>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<link href="https://survey.codes/styles.css" rel="stylesheet"/>
	  
	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,500|Roboto:400,500&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400&display=swap" rel="stylesheet"> 
</head>

<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-light bg-light">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>
		  
		  	<a class="navbar-brand" href="https://survey.codes/">survey.codes</a>

		  	<div class="collapse navbar-collapse" id="navbarTogglerDemo03">

				<div class="container">
					
			    	<ul class="navbar-nav mr-auto mt-2 mt-lg-0 justify-content-center">
			      		<li class="nav-item">
			        		<a class="nav-link" href="https://survey.codes/surveyamc">SurveyAMC</a>
			      		</li>

			      		<li class="nav-item">
			        		<a class="nav-link" href="https://survey.codes/click/">ClickAMC</a>
			      		</li>

			      		<li class="nav-item">
			        		<a class="nav-link" href="https://survey.codes/datatblr">Datatblr</a>
			      		</li>

						<li class="nav-item">
			        		<a class="nav-link" href="https://survey.codes/mendel">Mendel</a>
			      		</li>		      		
			    	</ul>		    
				</div>
			</div>
				<img class="d-none d-sm-none d-md-block" src="https://survey.codes/img/mendel-logo6.svg" alt="mendel-logo" style="opacity:0; width:8%; margin:20px">
		</nav>
	</header>

	<div class="container">  				  	
	  	<div id="column">
		    <h2 class="mt-3" style="text-align: center">404</h2>
		    <p>
				We can't find the page you're looking for.
		    </p>
		</div>
	</div>	


	<div class="card text-center" style="margin-top: 50px;">
  		
  		<div class="card-header">    
  		</div>

  		<div class="card-body">
    		<h5 class="card-title">Contact</h5>
    		<p style="text-align: center;">
    			<a href="https://www.uni-potsdam.de/soziologie-methoden/saalbach.php" target="_blank" style="color: #484F56 !important;">Claudia Saalbach</a> & 
    			<a href="https://www.uni-potsdam.de/soziologie-methoden/schuett.php" target="_blank" style="color: #484F56 !important;">Johannes Schütt</a>, 
    			<a href="https://www.uni-potsdam.de" target="_blank" style="color: #484F56 !important;">University of Potsdam</a>	
    		</p>
    		
  		</div>
  
  		<div class="card-footer text-muted">
    		<a href="https://www.uni-potsdam.de/soziologie-methoden/impressum.php" target="_blank" style="color: #484F56 !important;">Impressum</a>
  		</div>
	</div>





    <!-- jQuery (Bootstrap JS plugins depend on it) -->
    <script src="jsn/jquery-3.1.0.min.js"></script>
    <script src="jsn/bootstrap.min.js"></script>
    <script src="jsn/script.js"></script>
 </body>
</html>
