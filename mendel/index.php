<!-- Mendel stable-1.0                                                
Johannes Schütt, Claudia Saalbach
Chair of Empirical Methods, University of Potsdam 
License: GNU General Public License v2.0 only     
 -->

<?php
error_reporting(-1); # DEBUGGING

define('ABSPATH', dirname(__FILE__).'/');

if ($_SERVER['HTTP_HOST'] != "survey.codes") {
        header("Location: https://survey.codes/mendel");
}

## start function
function startsWith($haystack, $needle) {
  $length = strlen($needle);
  return (substr($haystack, 0, $length) === $needle);
}

# dir name function
function getName($n) {
 $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
 $randomString = '';

 for ($i = 0; $i < $n; $i++) {
     $index = rand(0, strlen($characters) - 1);
     $randomString .= $characters[$index];
 }

 return $randomString;
}

## preparation folder
$dir = getName(4); # create random 4-digit folder name

while (file_exists("tmp/" . $dir)) { # check if folder name is already taken
    $dir = getName(4);
}

# Upload file path
$target_data_file = "tmp/" . $dir . "/csv/data.csv";
$target_meta_file = "tmp/" . $dir . "/csv/meta.csv";

$err = ""; # error message
$suc = ""; # success message

# Send button got clicked
if (isset($_POST['btn'])) {

  if ($_FILES["datafile"]["name"] == "" || $_FILES["datafile"]["name"] == "") { # no file selected
    $err = ' 
            <div class="row justify-content-center error">
              No file(s) selected.
            </div>
           ';
    goto a;
  }
  
  $dataFileInfo = pathinfo($_FILES["datafile"]["name"]);
  $dataExt = strtolower($dataFileInfo["extension"]); # get data file extension

  $metaFileInfo = pathinfo($_FILES["metafile"]["name"]);
  $metaExt = strtolower($metaFileInfo["extension"]); # get meta file extension

  if (($dataExt != "csv" && $dataExt != "tsv") || ($metaExt != "csv" && $metaExt != "tsv")) { # check file type
    $err = ' 
            <div class="row justify-content-center error">
              The selected file is not a CSV/TSV file.
            </div>
            ';
    goto a;
  }
  else if ($_FILES["datafile"]["size"] > 3000000 || $_FILES["metafile"]["size"] > 3000000) { # check file size
    $err = ' 
            <div class="row justify-content-center error">
              One or both of the selected files are bigger then 3MB.
            </div>
           ';
    goto a;
  }

  switch($_POST["table_type"]) {
    case 1:
      $table_type = "means";

      $ci = FALSE;
      $statistics = [];
      $available = ["obs", "med", "ptiles", "mean", "sd", "ci", "min", "max", "mode", "perc"];
  
      for ($i = 1; $i <= 10; $i++) {
        if (in_array($_POST['s'.$i], $available)) {
          array_push($statistics, $_POST['s'.$i]);
          if ($_POST['s'.$i] == "ci") {
            $ci = TRUE;
          }
        }
      }
  
      $statistics = (implode(",", $statistics));

      break;
    case 2:
      $table_type = "percentages";
      break;
    default:
      $err = ' 
            <div class="row justify-content-center error">
              Unknown table type!
           </div>
           ';
      goto a;
      break;
  }

  $check = "id\tclass\ttype/scale\tname\ttext"; # column names of file
  $path = "tmp/" . $dir; # path of new folder
  $del = "rm -r " . $path; # preparing remove command for tmp dir in case of error

  mkdir($path); # create folder
  mkdir($path . "/csv");

  if (!(move_uploaded_file($_FILES["datafile"]["tmp_name"], $target_data_file) && move_uploaded_file($_FILES["metafile"]["tmp_name"], $target_meta_file))) {
    $err = ' 
            <div class="row justify-content-center error">
              Something went wrong...
            </div>  
           ';
    goto a;
  }

  $h = fopen($target_meta_file, "r"); # open handle
  $firstline = fgets($h, 1000); # get column names of upload
  fclose($h); # close handle

  if (!(startsWith($firstline, $check))) { # strings don't match
    $err = "";
    goto a;
  }
  
  # write job.csv
  $h = fopen($path . "/csv/job.csv", 'a');
  fwrite($h, "key\tvalue\n" .
             "data_file\tcsv/data.csv\n" .
             "meta_file\tcsv/meta.csv\n" .
             "table_type\t" . $table_type . "\n" .
             "dependend_vars\t" . $_POST["dependend"] . "\n" .
             "independend_vars\t" . $_POST["independend"] . "\n" .
             "");

  if ($table_type == "means") {
    fwrite($h, "statistical_values\t" . $statistics . "\n");

    if ($ci) {
      fwrite($h, "ci_level\t" . $_POST["ci_level"]. "\n");
    }
  }

  if ($_POST["caption"] != "") {
    fwrite($h, "caption\t" . $_POST["caption"] . "\n");
  }

  if ($_POST["footer"] != "") {
    fwrite($h, "footer\t" . $_POST["footer"] . "\n");
  }

  fclose($h);

  # copy shell program to dir
  mkdir($path . "/mndl_core");

  copy("scripts/import.R", $path . "/mndl_core/import.R");
  copy("scripts/main.R", $path . "/mndl_core/main.R");
  copy("scripts/prepare_job.R", $path . "/mndl_core/prepare_job.R");
  copy("scripts/write_output.R", $path . "/mndl_core/write_output.R");

  if ($table_type == "means") {
    mkdir($path . "/mndl_core/means");
    copy("scripts/means/functions.R", $path . "/mndl_core/means/functions.R");
    copy("scripts/means/generate.R", $path . "/mndl_core/means/generate.R");
    copy("scripts/means/options.R", $path . "/mndl_core/means/options.R");
  }
  else {
    mkdir($path . "/mndl_core/percentages");
    copy("scripts/percentages/functions.R", $path . "/mndl_core/percentages/functions.R");
    copy("scripts/percentages/generate.R", $path . "/mndl_core/percentages/generate.R");
    copy("scripts/percentages/options.R", $path . "/mndl_core/percentages/options.R");
  }

  # permissions
  chmod($path, 0777);

  chdir($path); # move to dir
  exec("Rscript mndl_core/main.R"); # run shell program ####

  if (!file_exists(ABSPATH. $path . "/twoway.pdf") OR !file_exists(ABSPATH . $path . "/twoway.tex")) {

    $err = ' 
            <div class="row justify-content-center error">
              An error ocurred. Please check your file for illegal characters.
            </div>  
              ';
    chdir("../../"); # switch back to root
    exec($del); # removing tmp
  }
  else {

    $zip = new ZipArchive;
    if ($zip->open("twoway.zip", ZipArchive::CREATE) === TRUE) {

      # add files to ZIP
      $zip->addFile('twoway.pdf');
      $zip->addFile('twoway.tex');

      $zip->close(); # close ZIP
    }

    $suc = '
          <div class="row justify-content-center">
            <div class="mx-auto" style="              
              width:600px;
              text-align: center; 
              margin-bottom: 5%;              
              color: #2C3744; 
              border: 1px dashed #28a745;
              border-radius: 5px;
              padding-top: 1.5%;
              padding-bottom: 1.5%;
              font-weight: 550;">
                Woohoo! Your tables are created. 
                <br>
                You can download them here:
                <br>                
                
                <a href="' . $path . '/twoway.zip"><button style="margin-top:2%;" type="button" class="btn btn-success">Download</button>
                </a>     
            </div>
           </div> 
        ';
  }


  # Skip to end (very bad, i know...)
  a:
}
?>

<!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="utf-8"/>
	<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
	<meta content="width=device-width, initial-scale=1" name="viewport"/>

	<title>Mendel</title>

	<link href="../favicon.ico" rel="shortcut icon" type="image/x-icon"/>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<link href="../styles.css" rel="stylesheet"/>

	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,500|Roboto:400,500&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400&display=swap" rel="stylesheet">
    <style type="text/css">
     .row {
        margin-left: -5px;
        margin-right: -5px;        
      }
      .row > form {
        border: 1px solid #F3C71E;
        border-radius: 5px;
        margin-top: 2.5%;
        margin-bottom: 2.5%;
        padding:5%; 
      }
        
    .formbox {
        border: 1px solid #F3C71E;
        border-radius: 5px;
      }  
    .btn-primary {
        color: #fff;
        background-color: #007bff;
        border-color: #007bff;
      }

      .btn-primary:hover {
        color: #fff;
        background-color: #0069d9;
        border-color: #0062cc;
      }

      .btn-primary:focus, .btn-primary.focus {
        color: #fff;
        background-color: #0069d9;
        border-color: #0062cc;
        box-shadow: 0 0 0 0.2rem rgba(38, 143, 255, 0.5);
      }

      .error{
        text-align: center; 
        margin-top: 2.5%;
        margin-bottom:5%;        
        color: #CD4F39; 
        font-size: 80%;
        border: 1px dashed #CD4F39;
        border-radius: 5px;
        padding: 3%;        
      }      
  </style>
</head>

<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-light bg-light">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>

		  	<a class="navbar-brand" href="https://survey.codes/">survey.codes</a>

		  	<div class="collapse navbar-collapse" id="navbarTogglerDemo03">

				<div class="container">

			    	<ul class="navbar-nav mr-auto mt-2 mt-lg-0 justify-content-center">
			      		<li class="nav-item">
			        		<a class="nav-link" href="../surveyamc">SurveyAMC</a>
			      		</li>

			      		<li class="nav-item">
			        		<a class="nav-link" href="../click">ClickAMC</a>
			      		</li>

			      		<li class="nav-item">
			        		<a class="nav-link" href="../datatblr">Datatblr</a>
			      		</li>

						<li class="nav-item active">
			        		<a class="nav-link" href="../mendel">Mendel</a>
			      		</li>
			    	</ul>
				</div>
			</div>
			<img class="d-none d-sm-none d-md-block" src="../img/mendel-logo6.svg" alt="mendel-logo" style="width:8%; margin:20px">
		</nav>
	</header>

	<div class="container">
	  	<div id="column">

        <!--messages-->
          <?php echo $err ;?>      
          <?php echo $suc ;?>      
        <!--end messages--> 

		    <h2 class="mt-3" style="text-align: center">Mendel</h2>
		    <p>
				  Mendel is a web application that generates fully labeled twoway tables of metric or categorical variables of a survey response dataset. After uploading the metadata of the questionnaire and the response dataset, a user interface allows to choose the dependent and independent variables and the statistics you want to present in your table. Mendel delivers the twoway tables as TEX and PDF files.
		    </p>
		</div>
	</div>

  <div class="row justify-content-center" style="margin-top:3%; font-family: Open Sans">    
      <a href="https://survey.codes/mendel/examples/mendel.zip"><button type="button" class="btn btn-outline-primary btn-lg">Download Demo Files</button></a>
    
  </div>  

  <div class="container">  				  	
	  <div id="column">
      <div class="row justify-content-center">         

        <div class="col-sm col-lg-6">
		      <h2 class="mt-3" style="text-align: center;">Gallery</h2>	
        </div>  
			  
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			  	<ol class="carousel-indicators">
			    	<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
			    	<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>			    	
			    	<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			  	</ol>
			 
			  <div class="carousel-inner" style=" background-color: #f8f9fa !important; border: 1px solid #F3C71E;">
			    
			   	<div class="carousel-item active">
				   	<a href="../pdf/kksoep_1.pdf"><img src="../img/kksoep_1.jpg" class="d-block w-50" alt="questionnaire example 1"></a>
				  </div>
				  <div class="carousel-item">
				  	<a href="../pdf/kksoep_2.pdf"><img src="../img/kksoep_2.jpg" class="d-block w-50" alt="questionnaire example 2"></a>
				  </div>
				  <div class="carousel-item">
				  	<a href="../pdf/kksoep_3.pdf"><img src="../img/kksoep_3.jpg" class="d-block w-50" alt="questionnaire example 3"></a>
				  </div>
				  <div class="carousel-item">
				   	<a href="../pdf/kksoep_4.pdf"><img src="../img/kksoep_4.jpg" class="d-block w-50" alt="questionnaire example 4"></a>
				  </div>
          <div class="carousel-item">
				   	<a href="../pdf/kksoep_5.pdf"><img src="../img/kksoep_5.jpg" class="d-block w-50" alt="questionnaire example 5"></a>
				  </div>
			 
  				<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
  				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
  				    <span class="sr-only">Previous</span>
  				</a>
			  
			  	<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
			    	<span class="carousel-control-next-icon" aria-hidden="true"></span>
			    	<span class="sr-only">Next</span>
			  	</a>
			  </div>

  		</div>
    </div>
  </div>

  <div class="container">
    <div id="column">
      <!--form row-->
      <div class="row justify-content-center" id="App" style="margin-top: 5%;">  

        <!--section form-->
        <div class="col-12">
          <h3 class="mt-3" style="text-align: center;">Upload your Source Files</h3>
        </div>
          
        <!--form col-->
        <div class="formbox col-sm col-lg-6" style="padding:5%">

	        <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data" style="font-family: 'Open Sans';">

	          <!--browse data file-->
            <div class="form-group">
	            <div class="custom-file">
	              <input type="file" onchange="getDataFileName()" id="datafile" class="custom-file-input" name="datafile" accept="text/csv" required>
	              <label style="overflow: hidden;" id="datafile_display" class="custom-file-label" for="name">Choose data file</label>
	            </div>
	          </div>

	          <!--browse meta file-->
            <div class="form-group">
	            <div class="custom-file">
	              <input type="file" onchange="getMetaFileName()" id="metafile" class="custom-file-input" name="metafile" accept="text/csv" required>
	              <label style="overflow: hidden;" id="metafile_display" class="custom-file-label" for="name">Choose meta file</label>
	            </div>
	          </div>

            <!--table types-->
            <div class="form-group">	            
	            <!--option 1-->
	            <div class="form-check form-check-inline">
	              <input type="radio" value="1" class="form-check-input" id="option1" name="table_type" checked>
	              <label class="form-check-label" for="option1">means</label>
	            </div>

	            <!--option 2-->
	            <div class="form-check form-check-inline">
	              <input type="radio" value="2" class="form-check-input" id="option2" name="table_type">
	              <label class="form-check-label" for="option2">percentages</label>
	            </div>
	          </div>

            <!--dependend-->
            <div class="form-group">                                      
              <span class="input-group-addon"></span>
              <input id="dependend" type="text" class="form-control" name="dependend" placeholder="Dependend variables" required>            
              <p style="font-size: 70%;">For example: hsat,lsat,dsat</p>
            </div>

            <!--independend-->
            <div class="form-group">                                      
              <span class="input-group-addon"></span>
              <input id="independend" type="text" class="form-control" name="independend" placeholder="Independend variables" required>
              <p style="font-size: 70%;">For example: state,mar,edu</p>
            </div>

            <!--statistics-->
            <div id="statistics-block" style="display: block">
            <p>Statistics</p>
              <div class="form-group">  
              <div class="row">
                  <div class="col-sm col-lg-6">
                    <select class="form-control form-control-sm" name="s1" id="s1">
                      <option value="NA">&lt;none&gt;</option>
                      <option selected value="obs">Observations</option>
                      <option value="med">Median</option>
                      <option value="ptiles">Percentiles</option>
                      <option value="mean">Mean</option>
                      <option value="sd">Standard deviation</option>
                      <option value="ci">Confidence Interval</option>
                      <option value="min">Minima</option>
                      <option value="max">Maxima</option>
                      <option value="mode">Mode</option>
                      <option value="perc">Percentage</option>
                    </select>
                  </div>
                  <div class="col-sm col-lg-6">
                    <select class="form-control form-control-sm" name="s2" id="s2">
                      <option value="NA">&lt;none&gt;</option>
                      <option value="obs">Observations</option>
                      <option selected value="med">Median</option>
                      <option value="ptiles">Percentiles</option>
                      <option value="mean">Mean</option>
                      <option value="sd">Standard deviation</option>
                      <option value="ci">Confidence Interval</option>
                      <option value="min">Minima</option>
                      <option value="max">Maxima</option>
                      <option value="mode">Mode</option>
                      <option value="perc">Percentage</option>
                    </select>
                  </div>
                </div>  

                <div class="row">
                  <div class="col-sm col-lg-6">
                    <select class="form-control form-control-sm" name="s3" id="s3">
                      <option value="NA">&lt;none&gt;</option>
                      <option value="obs">Observations</option>
                      <option value="med">Median</option>
                      <option value="ptiles">Percentiles</option>
                      <option selected value="mean">Mean</option>
                      <option value="sd">Standard deviation</option>
                      <option value="ci">Confidence Interval</option>
                      <option value="min">Minima</option>
                      <option value="max">Maxima</option>
                      <option value="mode">Mode</option>
                      <option value="perc">Percentage</option>
                    </select>
                  </div>
                  <div class="col-sm col-lg-6">
                    <select class="form-control form-control-sm" name="s4" id="s4">
                      <option value="NA">&lt;none&gt;</option>
                      <option value="obs">Observations</option>
                      <option value="med">Median</option>
                      <option value="ptiles">Percentiles</option>
                      <option value="mean">Mean</option>
                      <option selected value="sd">Standard deviation</option>
                      <option value="ci">Confidence Interval</option>
                      <option value="min">Minima</option>
                      <option value="max">Maxima</option>
                      <option value="mode">Mode</option>
                      <option value="perc">Percentage</option>
                    </select>
                  </div>
                </div>  
              <div class="d-none d-sm-block">
                <div class="row">
                  <div class="col-sm col-lg-6">
                    <select class="form-control form-control-sm" name="s5" id="s5">
                      <option selected value="NA">&lt;none&gt;</option>
                      <option value="obs">Observations</option>
                      <option value="med">Median</option>
                      <option value="ptiles">Percentiles</option>
                      <option value="mean">Mean</option>
                      <option value="sd">Standard deviation</option>
                      <option value="ci">Confidence Interval</option>
                      <option value="min">Minima</option>
                      <option value="max">Maxima</option>
                      <option value="mode">Mode</option>
                      <option value="perc">Percentage</option>
                    </select>
                  </div>
                  <div class="col-sm col-lg-6">
                    <select class="form-control form-control-sm" name="s6" id="s6">
                      <option selected value="NA">&lt;none&gt;</option>
                      <option value="obs">Observations</option>
                      <option value="med">Median</option>
                      <option value="ptiles">Percentiles</option>
                      <option value="mean">Mean</option>
                      <option value="sd">Standard deviation</option>
                      <option value="ci">Confidence Interval</option>
                      <option value="min">Minima</option>
                      <option value="max">Maxima</option>
                      <option value="mode">Mode</option>
                      <option value="perc">Percentage</option>
                    </select>
                  </div>
                </div>    

                <div class="row">
                  <div class="col-sm col-lg-6">
                    <select class="form-control form-control-sm" name="s7" id="s7">
                      <option selected value="NA">&lt;none&gt;</option>
                      <option value="obs">Observations</option>
                      <option value="med">Median</option>
                      <option value="ptiles">Percentiles</option>
                      <option value="mean">Mean</option>
                      <option value="sd">Standard deviation</option>
                      <option value="ci">Confidence Interval</option>
                      <option value="min">Minima</option>
                      <option value="max">Maxima</option>
                      <option value="mode">Mode</option>
                      <option value="perc">Percentage</option>
                    </select>
                  </div>
                  <div class="col-sm col-lg-6">
                    <select class="form-control form-control-sm" name="s8" id="s8">
                      <option selected value="NA">&lt;none&gt;</option>
                      <option value="obs">Observations</option>
                      <option value="med">Median</option>
                      <option value="ptiles">Percentiles</option>
                      <option value="mean">Mean</option>
                      <option value="sd">Standard deviation</option>
                      <option value="ci">Confidence Interval</option>
                      <option value="min">Minima</option>
                      <option value="max">Maxima</option>
                      <option value="mode">Mode</option>
                      <option value="perc">Percentage</option>
                    </select>
                   </div>
                  </div>  

                  <div class="row">
                    <div class="col-sm col-lg-6">                                    
                      <select class="form-control form-control-sm" name="s9" id="s9">
                        <option selected value="NA">&lt;none&gt;</option>
                        <option value="obs">Observations</option>
                        <option value="med">Median</option>
                        <option value="ptiles">Percentiles</option>
                        <option value="mean">Mean</option>
                        <option value="sd">Standard deviation</option>
                        <option value="ci">Confidence Interval</option>
                        <option value="min">Minima</option>
                        <option value="max">Maxima</option>
                        <option value="mode">Mode</option>
                        <option value="perc">Percentage</option>
                      </select>
                    </div>
                    <div class="col-sm col-lg-6 d-sm-block">                                    
                      <select class="form-control form-control-sm" name="s10" id="s10">
                        <option value="NA">&lt;none&gt;</option>
                        <option value="obs">Observations</option>
                        <option value="med">Median</option>
                        <option value="ptiles">Percentiles</option>
                        <option value="mean">Mean</option>
                        <option value="sd">Standard deviation</option>
                        <option value="ci">Confidence Interval</option>
                        <option value="min">Minima</option>
                        <option value="max">Maxima</option>
                        <option value="mode">Mode</option>
                        <option value="perc">Percentage</option>
                      </select>
                    </div>
                  </div>  
                 </div>                                   
              </div>  

            <!--ci level-->
            <div id="ci-level-block" style="display: none;">
              <p>CI Level</p>            
              <div class="form-group">                
              <!--option 1-->
                <div class="form-check form-check-inline" style="margin-right: 10px;">
                  <input type="radio" value="0.01" class="form-check-input" id="option1" name="ci_level">
                  <label class="form-check-label" for="option1">1 %</label>
                </div>                  
                <!--option 2-->
                <div class="form-check form-check-inline" style="margin-right: 10px;">
                  <input type="radio" value="0.025" class="form-check-input" id="option2" name="ci_level">
                  <label class="form-check-label" for="option2">2.5 %</label>
                </div>
                <!--option 3-->
                <div class="form-check form-check-inline" style="margin-right: 10px;">
                  <input type="radio" value="0.05" class="form-check-input" id="option3" name="ci_level" checked>
                  <label class="form-check-label" for="option3">5 %</label>
                </div>
                <!--option 4-->
                <div class="form-check form-check-inline" style="margin-right: 10px;">
                  <input type="radio" value="0.1" class="form-check-input" id="option4" name="ci_level">
                  <label class="form-check-label" for="option4">10 %</label>
                </div>              
              </div>
            </div>
          </div>
            <!--caption-->
            <div class="form-group">
              <span class="input-group-addon"></span>
              <input id="caption" type="text" class="form-control" name="caption" placeholder="Caption (leave blank for standard)">
            </div>

            <!--footer-->
            <div class="form-group">
              <span class="input-group-addon"></span>
              <input id="footer" type="text" class="form-control" name="footer" placeholder="Footer (leave blank for standard)">
            </div>

	          <!--send-->
            <div class="form-group">                
	            <div class="input-group-append">
	              <button class="btn btn-outline-primary" type="submit" name="btn">Send</button>
	            </div>
	          </div>

	        </div>
	      </form>
      </div>
    </div>
   </div>
  </div>  

    <div class="container">
      <div id="column">
          <div class="row justify-content-center">
            <div class="col-sm col-lg-6">
              <h3 class="mt-3" style="text-align: center;">Sample Source Files</h3>
              <p>
              Try out Mendel with our sample source files. The data file contains the questionnaires' response data, and the meta file includes the questionnaires' meta information. Download them and then upload them again using the form above.
              </p> 
             </div> 
              
            <div class="col-12" style="text-align: center; font-family: Open Sans;">                        
              <a href="../datatblr/examples/data.csv">
                <button type="button" class="btn btn-outline-primary btn-sm" style="width:100px;">data.csv</button>
              </a>
              <a href="../datatblr/examples/meta.tsv">
                <button type="button" class="btn btn-outline-primary btn-sm" style="width:100px;">meta.tsv</button>
              </a>        
            </div>      

            <div class="col-sm col-lg-6" style="margin-top: 2%">
              <p>
              You may also take a look at the meta file to get to know its' structure. Further information about how to create a meta file you will find in the SurveyAMC <a href="https://survey.codes/pdf/surveyamc_manual.pdf" target="_blank">Manual</a>.
              </p>       
            </div>
              
          </div>
        </div>
       </div> 

    <div class="container">
      <div id="column">
        <div class="row justify-content-center">
          <div class="col-sm col-lg-6">
            <h3 class="mt-3" style="text-align: center;">Desktop Version</h3>

            <p>
            Mendel is also available as a desktop version if you want to use it locally on your device. The <a href="https://github.com/johschuett/Mendel" target="_blank">Mendel repository</a> provides further information about installation and usage. </p>
          </div>
        </div>    
      </div>  
    </div>

    <div class="container">
      <div id="column">    
        <div class="row justify-content-center">
          <div class="col-sm col-lg-6">
            <h3 class="mt-3" style="text-align: center;">Fork Mendel</h3>
            <p>
            The source code is freely available via the <a href="https://gitlab.com/CSaalbach/survey.codes" target="_blank">Mendel repository</a>. There you can download and modify the program.
            </p> 
          </div>  
        </div>  
      </div>  
    </div>  

  </div>
</div>

  <div class="card text-center" style="margin-top: 50px;">
      
      <div class="card-header">    
      </div>

      <div class="card-body">
        <h5 class="card-title">Contact</h5>
        <p style="text-align: center;">
    <a href="https://www.uni-potsdam.de/soziologie-methoden/" target="_blank" style="color: #484F56 !important;">Lehrstuhl Methoden der empirischen Sozialforschung</a>,
    <a href="https://www.uni-potsdam.de" target="_blank" style="color: #484F56 !important;">University of Potsdam</a> 
        </p>
        
      </div>
  
      <div class="card-footer text-muted">
        <a href="https://www.uni-potsdam.de/de/soziologie-methoden/impressum" target="_blank" style="color: #484F56 !important;">Impressum</a>
      </div>
  </div>

  <script>
  // Check if table type is mean or percentages on load
  var val = $("input[type='radio'][name=table_type]:checked").val();

  if (val == 1) {
    $('#statistics-block').css('display', 'block');
  } else {
    $('#statistics-block').css('display', 'none');
  }

  // Check if ci exists on load
  function checkAll() {
   var i;
   for(i = 1; i <= 10; i++) {
     var current_s = document.getElementById('s' + i);
      if(current_s.value == 'ci') {
        $('#ci-level-block').css('display', 'block');
        break;
      }
      $('#ci-level-block').css('display', 'none');
   }
  }

  checkAll();

  // Check if table type is mean or percentages on change
  $('input[type=radio][name=table_type]').change(function() {
    if(this.value == 1) {
      $('#statistics-block').css('display', 'block');
    } else if(this.value == 2) {
      $('#statistics-block').css('display', 'none');
    }
  });

  function checkAllonChange(index) {
    var select = document.getElementById('s' + index);
     if(select.value == 'ci') {
       $('#ci-level-block').css('display', 'block');
     } else {
       checkAll();
     }
  }

  $('#s1').change(function() {
    checkAllonChange(1);
  });

  $('#s2').change(function() {
    checkAllonChange(2);
  });

  $('#s3').change(function() {
    checkAllonChange(3);
  });

  $('#s4').change(function() {
    checkAllonChange(4);
  });

  $('#s5').change(function() {
    checkAllonChange(5);
  });

  $('#s6').change(function() {
    checkAllonChange(6);
  });

  $('#s7').change(function() {
    checkAllonChange(7);
  });

  $('#s8').change(function() {
    checkAllonChange(8);
  });

  $('#s9').change(function() {
    checkAllonChange(9);
  });

  $('#s10').change(function() {
    checkAllonChange(10);
  });

  </script>

    <!-- jQuery (Bootstrap JS plugins depend on it) -->
    <!-- <script src="../jsn/jquery-3.1.0.min.js"></script> -->
    <!-- <script src="../jsn/bootstrap.min.js"></script> -->
    <!-- <script src="../jsn/script.js"></script> -->

    <script>
      function getDataFileName() {
        var fullPath = document.getElementById('datafile').value;
        if (fullPath) {
           var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
             var filename = fullPath.substring(startIndex);
                   if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                   filename = filename.substring(1);
               }
               document.getElementById('datafile_display').innerHTML = filename;
        }
      }

      function getMetaFileName() {
        var fullPath = document.getElementById('metafile').value;
        if (fullPath) {
           var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
             var filename = fullPath.substring(startIndex);
                   if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                   filename = filename.substring(1);
               }
               document.getElementById('metafile_display').innerHTML = filename;
        }
      }
    </script>
 </body>
</html>
