

**The survey.codes project | <https://survey.codes>**

Claudia Saalbach, Johannes Schütt | Lehrstuhl für Methoden der empirischen Sozialforschung, Universität Potsdam | April, 2021


[[_TOC_]]

# What is it about?
The survey.codes project demonstrates how you can use metadata in various steps during the survey lifecycle to generate several survey products like machine readable questionnaires, survey documentation files and table volumes. The project includes four software components: **SurveyAMC**,  **ClickAMC**, **Datatblr** and **Mendel**. This repository hosts the web presence, which you can visit here: <https://survey.codes>. Further, there exists a desktop version, so that you can use ClickAMC, Datatblr and Mendel as a bundle locally via your console (see <https://github.com/johschuett>).

Survey.codes is a free software, distributed under the GNU General Public License v2.0 only.

# SurveyAMC
With SurveyAMC you can create machine readable questionnaires for self-administered paper surveys in high typesetting quality. SurveyAMC is available as a package option of the [Auto-Multiple-Choice Software](https://auto-multiple-choice.net). The included LaTeX package (`automultiplechoice.sty`) provides a style for designing the questionnaire's general layout, as well as for formatting and placing the questions, the answers, the answer boxes, the completion and filter instructions. After the completion of the survey, the respondents' answers can be automatically processed into a `.csv` file and used for further analysis with common software packages. 

## Usage
To create machine readable questionnaires with SurveyAMC, you will need a LaTeX distribution, a LaTeX editor, a `.pdf` viewer and the Auto-Multiple-Choice Software, or alternatively the implemented LaTeX package: both you can download via the Auto-Multiple-Choice GitLab repository (<https://gitlab.com/jojo_boulix/auto-multiple-choice>). You can use the use the SurveyAMC option by calling `\usepackage[survey]{automultiplechoice}` within the preamble of your LaTeX document. For more information about the package options view the [SurveyAMC Manual](https://survey.codes/pdf/surveyamc_manual.pdf).


# ClickAMC
ClickAMC helps you get started with your own paper based survey project. The ClickAMC app generates from one meta file several survey products, like the machine readable LaTeX questionnaire, the data dictionary, and the Stata `.do` file for labeling the answer dataset. All you need to do is import a tab separated file (`.tsv, .csv, .txt`) that contains the metadata of the questionnaire. The metafiles' structure is adapted from the online survey software Limesurvey, so that you can also generate a web questionnaire from the same source. For more information about the metadata files' structure, take a look at the chapter "Work with Structured Metafiles" of the [SurveyAMC Manual](<https://survey.codes/pdf/surveyamc_manual.pdf>). Alternatively, you can download several sample meta files from the survey.codes web page (<https://survey.codes/click>). For processing the filled answer sheets to a dataset (`.csv`), you can use the Auto-Multiple-Choice Software. 

## Usage
You can use ClickAMC via the web application <https://survey.codes/click>. Further, you can run the `.R` script that generates the `.tex` code for the questionnaire based on a questionnaires' metafile locally on your computer. You will find the `.R` script in the repositories' directory `click\scripts\`. Within the script `quest_generator.R` you can adjust the path to your meta file and set some options manually:

* For this, uncomment the line `options <- read.csv(file="csv/options.csv", header=TRUE, sep=",")` and replace the value assignment for the objects `layout` and `missing` by integers. 

  + `layout`: There are six different questionnaire layouts available, so you can choose an integer between `1` and `6`. 
  
  + `missing`: As a missing value, you can choose any integer you like, for example `99`. 

* Besides, take care that your metafile is located in your R working directory and that the `automultiplechoice.sty` LaTeX package is installed or alternatively also stored in your working directory. 

Further, you can use ClickAMC locally via your console. For this, visit <https://github.com/johschuett>.

### Dependencies

* R > 3.6.1
  
* TeX Live: automultiplechoice, csvsimple, tikz, tcolorbox, fontawesome, amssymb, eurosym, lmodern, fetamont, roboto, sourcesanspro, lato, crimson


# Datatblr
Datatblr generates a table volume that contains the univariate description of all metric and categorical variables of a data set. Datatblr delivers the table volume as LaTeX and `.pdf` file. All you have to do is upload a `.csv`  file with your survey response data and a tab separated file (`.tsv, .csv, .txt`) with the meta information of your questionnaire. Datatblr is suitable for generating table volumes that typically appear in the appendix of research reports. 

## Usage
You can use Datatblr via the web application <https://survey.codes/datablr>. Further, you can run the `.R` script that generates the `.tex` code for the table volumes based on a meta file and an answer dataset locally on your computer. You will find the `.R` scripts in the repositories' directory `datatablr\scripts\`. Edit the script `import.R` to adjust the paths to the meta and the data file. Further you can set some options manually: 

* Exchange the path to the file `options.csv` with the integer `1` or `2` to choose between *summarize missings* or *ignore missings*.

Further, you can use Datatblr locally via your console. For this, visit <https://github.com/johschuett>.

### Dependencies

* R > 3.6.1: dplyr, rio
  
* TeX Live: amssymb, array, booktabs, dcolumn, geometry, ltxtable, multirow, tabularx

# Mendel
Mendel generates fully labeled twoway tables of metric or categorical variables of a survey response dataset. After importing the metadata and the response dataset, a user interface allows to choose the dependent and independent variables and the statistics you want to present in your table. Mendel delivers the twoway tables as `.tex` and `.pdf` files. 

## Usage
You can use Mendel via the web application <https://survey.codes/mendel>. Further, you can run the `.R` scripts that generate the `.tex` code for the table volumes based on a meta file and an answer dataset locally on your computer. For this, we recommend using the console version of survey.codes, which is available here: <https://github.com/johschuett>.

### Dependencies

* R > 3.6.1: dplyr, rio
  
* TeX Live: amssymb, array, booktabs, dcolumn, geometry, ltxtable, multirow, tabularx


  
