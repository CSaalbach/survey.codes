<?php

error_reporting(0); # DEBUGGING

define('ABSPATH', dirname(__FILE__).'/');
define('access', TRUE); # access to counter.php
include("gen_chars.php"); # include LaTeX chars

## check url
#if ($_SERVER['HTTP_HOST'] != "survey.codes") {
#        header("Location: https://survey.codes/click");
#}

## start function
function startsWith($haystack, $needle) {
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

function getName($n) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';

    for ($i = 0; $i < $n; $i++) {
        $index = rand(0, strlen($characters) - 1);
        $randomString .= $characters[$index];
    }

    return $randomString;
}
## end function
 
## preparation folder
$dir = getName(4); # create random 4-digit folder name

while (file_exists("tmp/" . $dir)) { # check if folder name is already taken
    $dir = getName(4);
}

#echo $dir;

$target_file = "tmp/" . $dir . "/content.csv";
$err = ""; # error message
$suc = ""; # success message
$title = "ClickAMC"; # title of page
$upload = 1;

## start procedure
if(isset($_POST['btn'])) { 

  if (true) {
    $fileInfo = pathinfo($_FILES["datei"]["name"]);
    $ext = strtolower($fileInfo["extension"]); # get file extension

    if ($_FILES["datei"]["name"] == "") { # no file selected
      $upload = 0;
      $err = "No file selected.";
    }
    else if ($ext != "csv" && $ext =! "txt") { # check file type
        //else if ($_FILES["datei"]["type"] != "text/csv" && $_FILES["datei"]["type"] != "text/plain" && $_FILES["datei"]["type"] != "text/comma-separated-values" && $_FILES["datei"]["type"] != "application/vnd.ms-excel") { # File is no csv file
      $upload = 0;
      $err = "The selected file is not a CSV or TXT file.";
    }
    else if ($_FILES["datei"]["size"] > 500000) { # check file size
      $upload = 0;
      $err = "The selected file is bigger then 500kb.";
    }

    if ($upload == 1) { # file passed check
      $check = "id\tclass\ttype/scale\tname\ttext"; # column names of file
      $path = "tmp/" . $dir; # path of new folder
      $del = "rm -r " . $path; # preparing remove command for tmp dir in case of error

      mkdir($path); # create folder

      if (move_uploaded_file($_FILES["datei"]["tmp_name"], $target_file)) { # move tempfile to new folder
      
        $h = fopen($target_file, "r"); # open handle
          $firstline = fgets($h, 1000); # get column names of upload
        fclose($h); # close handle

        if (startsWith($firstline, $check)) { # strings match
          mkdir($path . "/output"); # make output directory
          include("gen_stata.php"); # include stata labler
            
          $h = fopen($target_file, "r"); # open handle
            $file = file($target_file); # for count($file);
            for ($i = 0; $i <= count($file); $i++) {
              $line = fgets($h, 1000); # get current line
              $line = str_replace("\\", "\\textbackslash ", $line);
              $clean[] = $line; # put converted line in array
            }            
          fclose($h); # close handle

          $h = fopen($target_file, "w"); # open handle
            for ($i = 0; $i < count($clean); $i++) {
              $t = $clean[$i];
              fwrite($h, $t);
            }          
          fclose($h); # close handle, file has been overwritten

          
          if(isset($_POST['chars'])) { # check checkbox "chars"
            $h = fopen($target_file, "r"); # open handle
              $file = file($target_file); # for count($file);
              for ($i = 0; $i <= count($file); $i++) {
                $line = fgets($h, 1000); # get current line
                $line = str_replace(array_keys($chars), array_values($chars), $line); # replace special char with the LaTeX code from charconverter.php
                $data[] = $line; # put converted line in array
              }
            fclose($h); # close handle

            $h = fopen($target_file, "w"); # open handle
              for ($i = 0; $i < count($data); $i++) {
                $t = $data[$i];
                fwrite($h, $t);
              }
            fclose($h); # close handle, file has been overwritten
          } # end check checkbox "chars"
        } # end strings match

        # copy shell program to dir
        copy("scripts/autosurvey.sh", $path . "/autosurvey.sh");
        copy("scripts/gen_codebook.tex", $path . "/gen_codebook.tex");
        copy("scripts/gen_quest.tex", $path . "/gen_quest.tex");
        copy("scripts/surveyamc.sty", $path . "/surveyamc.sty");

        # permissions
        chmod($path, 0777);
        chmod($path ."/autosurvey.sh", 0777);
        chmod($path . "/gen_codebook.tex", 0777);
        chmod($path . "/gen_quest.tex", 0777);
        chmod($path . "/surveyamc.sty", 0777);
        chmod($path . "/content.csv", 0777);

        chdir($path); # move to dir
        exec("sh autosurvey.sh"); # run shell program #####

        if (!file_exists(ABSPATH. $path . "/output/codebook.pdf") OR !file_exists(ABSPATH . $path . "/output/gen_quest.pdf") OR !file_exists(ABSPATH . $path . "/output/questionnaire.pdf") OR !file_exists(ABSPATH . $path . "/output/label.do")) {

            if(isset($_POST['chars'])) { # check checkbox "chars"
              $err = 'An error ocurred. Please check your file for illegal characters.';
              chdir("../../"); # switch back to root
              exec($del); # removing tmp
            }
            else {
              $err = 'An error ocurred. Try <i>"Convert special characters to LaTeX code"</i>.';
              chdir("../../"); # switch back to root
              exec($del); # removing tmp
            }
        }

        else {
          $zipname = utf8_decode(basename($_FILES["datei"]["name"], ".csv")); # name ZIP with upload file name
          $zipname = preg_replace("/[^a-zA-Z0-9]+/", "", $zipname); # rename if original name contains only alphanumerical characters

          if($zipname == "") { # if CSV name contais non-alphanumerical characters, rename ZIP to "output.zip"
            $zipname = "output";
          }

          $zipname = $zipname . ".zip";

          $zip = new ZipArchive;
          if ($zip->open($zipname, ZipArchive::CREATE) === TRUE) {
            # add files to ZIP
            $zip->addFile('output/codebook.pdf');
            $zip->addFile('output/content.csv');
            $zip->addFile('output/gen_codebook.tex');
            $zip->addFile('output/gen_quest.pdf');
            $zip->addFile('output/questionnaire.tex');
            $zip->addFile('output/questionnaire.pdf');
            $zip->addFile('output/surveyamc.sty');
            $zip->addFile('output/label.do');

            $zip->close(); # close ZIP
          }
        }  

        chdir("../../"); # switch back to root
        include("counter.php");

        $title = "Wohoo!";

        $suc = '
          <div style="color: #2C3744;font-weight: 550;">
            <center>
              Woohoo! Your survey products were successfully generated. <br>
              You can download them as ZIP archive:
            </center>
            <br>
            <center>
              <a href="' . $path . '/' . $zipname . '"><button type="button" class="btn btn-primary" style="background-color:#F3C71E; border: 1px solid #F3C71E; width: 50%;">Download</button>
              </a>             
             </center> 
          </div>
        ';
      } # move tempfile
    } # check upload
    
    else { # strings dont match
      $err = "Please check if the column names of your TSV file are correct, and if your TSV file is tab-delimited. If you are not sure about your TSV file structure, you can take a look at the sample files below or the manual.";
        exec($del); # removing tmp dir
    }
  } # end if true    

  else { # upload failed
    $err = "Oops, something went wrong!";
    exec($del); # Removing tmp dir
  }  
} # end procedure
?>


<!doctype html>
<html>
  <head>
    <meta charset="utf-8"/>
    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>

    <title><?php echo $title; ?></title>
      
    <link href="../favicon.ico" rel="shortcut icon" type="image/x-icon"/>
      
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/bootstrap-filestyle.min.js"> </script>

    <link href="../styles.css" rel="stylesheet"/>
        
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,500|Roboto:400,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400&display=swap" rel="stylesheet">     
</head>

<body>
  <header>
    <nav class="navbar navbar-expand-md navbar-light bg-light">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>
      
        <a class="navbar-brand" href="https://survey.codes/">survey.codes</a>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">

        <div class="container">
          
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0 justify-content-center">
                <li class="nav-item">
                  <a class="nav-link" href="../surveyamc">SurveyAMC</a>
                </li>

                <li class="nav-item active">
                  <a class="nav-link" href="../click">ClickAMC</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="../datatblr">Datatblr</a>
                </li>

            <li class="nav-item">
                  <a class="nav-link" href="../mendel">Mendel</a>
                </li>             
            </ul>       
        </div>
      </div>
      <img class="d-none d-sm-none d-md-block" src="../img/camc-logo1.svg" alt="clickamc-logo" style="width:8%; margin:20px">
    </nav>
  </header>

  <div class="container">             
      <div id="column">
        <h2 class="mt-3" style="text-align: center">ClickAMC</h2>
        <p>
         ClickAMC helps you get started with your own paper based survey project. ClickAMC generates the machine readable LaTeX questionnaire, the data dictionary, and the Stata Do-File for labeling the answer dataset from one source file. All you need to do is to upload a TSV file that contains the metadata of the questionnaire. For more information about the source file structure, you can take a look at the <a href="https://survey.codes/pdf/surveyamc_manual.pdf" target="_blank" style="color: #2C3744;font-weight: 550;">Manual</a> of the SurveyAMC project. Alternatively, you can download several <a href="#SSF" style="color: #2C3744;font-weight: 550;">Sample Source Files</a> further down on the page. For processing the filled answer sheets to a dataset (CSV), you can use the <a href="https://www.auto-multiple-choice.net/" target="_blank" style="color: #2C3744;font-weight: 550;">Auto-Multiple-Choice Software</a>.
         </p>
      </div>
  </div>
          
  <div class="container">             
    <div id="column">
      <h3 class="mt-3" style="text-align: center;">Upload your Source File</h3>

        <div class="row justify-content-center">          
          <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
            <div class="input-group" style="font-family: 'Open Sans'">
                      
              <!--browse-->              
              <div class="custom-file">
                <input type="file" onchange="getFileName()" id="csvfile" class="custom-file-input" name="datei" accept="text/csv/tsv">
                <label style="overflow: hidden;" id="csvfile_display" class="custom-file-label" for="name">Choose file</label>
              </div>
              
              <!--send-->
              <div class="input-group-append">
                <button class="btn btn-outline-secondary" type="submit" name="btn">Send</button>              
              </div>
            </div>      

            <!--checkbox-->
            <div class="form-check" style="margin-top: 5%; font-family: 'Open Sans'">
              <input type="checkbox" class="form-check-input" name="chars">
              <label class="form-check-label" for="exampleCheck1">Convert special characters to LaTeX code.</label>
            </div>
          </form>

        </div>  
                    <?php echo
              "<div class='text-center'
                style='
                  color:#CD4F39;
                  margin-top:5%;
                  font-size:.8em;
                '>
                <p>
                ". $err ."
                </p>
              </div>"
            ;?>

            <?php echo
              "<div
                style='
                  margin-top:5%;
                  margin-bottom:5%;
                '>". $suc ."
              </div>"
            ;?>
      </div>    
    </div>


  <div class="container">             
    <div id="column">
      <h3 class="mt-3" style="text-align: center;">Notes</h3>
        <div class="row justify-content-center">
          <div style="width:400px;">
          <p>
           Please take care that your TSV file ... 
            <ul>
              <li> is tab separated,</li>
              <li> contains the column names <span id="robcon">id, class, type/scale, name</span> and <span id="robcon">title</span>,</li>
              <li> is not larger than 1 MB,</li>
              <li> does not contain any special characters except <a href="chartable.html" target="_blank" style="color: #2C3744;font-weight: 550;">these</a>.</li>
            </ul>
          </p>
        </div>
        </div>
     </div>
  </div>     

  <div class="container">             
    <div id="column">
      <h3 id="SSF" class="mt-3" style="text-align: center;">Sample Source Files</h3>
      <div class="row justify-content-center">
        <div style="width:400px;">
          <p>
            Try out ClickAMC with a sample source file. From the SurveyAMC Tutorial, the source file of the Marx questionnaire is available here in three languages. Download one of them and then upload the file again using the form above.

            <div class="list-group">
              <a href="examples/marx_en.tsv" class="list-group-item list-group-item-action text-center">
                english
              </a>
              <a href="examples/marx_de.tsv" class="list-group-item list-group-item-action text-center">
                german
              </a>
              <a href="examples/marx_fr.tsv" class="list-group-item list-group-item-action text-center">
                french
              </a>              
            </div>            
          </p>
        </div>
      </div>
    </div>        
  </div>  

  <div class="container">             
    <div id="column">
      <h3 class="mt-3" style="text-align: center;">Reuse ClickAMC</h3>
      <div class="row justify-content-center">
        <div style="width:400px;">
          <p>
            Use ClickAMC on your server. The source code is available via the <a href="https://gitlab.com/CSaalbach/clickamc-project" target="_blank" style="color: #2C3744;font-weight: 550;">ClickAMC repository</a>. There you can download and modify the program.           
          </p>
        </div>
      </div>
    </div>        
  </div>  


  <div class="card text-center" style="margin-top: 50px;">
      
      <div class="card-header">    
      </div>

      <div class="card-body">
        <h5 class="card-title">Contact</h5>
        <p style="text-align: center;">
          <a href="https://www.uni-potsdam.de/soziologie-methoden/saalbach.html" target="_blank" style="color: #484F56 !important;">Claudia Saalbach</a>, <a href="https://www.uni-potsdam.de" target="_blank" style="color: #484F56 !important;">Universität Potsdam</a> 
        </p>
        
      </div>
  
      <div class="card-footer text-muted">
        <a href="https://www.uni-potsdam.de/soziologie-methoden/impressum.html" target="_blank" style="color: #484F56 !important;">Impressum</a>
      </div>
  </div>


  <!-- jQuery (Bootstrap JS plugins depend on it) -->
  <script src="jsn/jquery-3.1.0.min.js"></script>
  <script src="jsn/bootstrap.min.js"></script>
  <script src="jsn/script.js"></script>



<!--<script> //CAPTCHA
  grecaptcha.ready(function() {
    grecaptcha.execute('6LcyjuUUAAAAAI_sSQ6AUxVQP2opjyJxl1HurKA_', {action: 'homepage'}).then(function(token) {
      console.log(token);
      document.getElementById("token").value = token;
    });
  });
</script>-->

  <script>
    function getFileName() {
      var fullPath = document.getElementById('csvfile').value;
      if (fullPath) {
         var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
           var filename = fullPath.substring(startIndex);
                 if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                 filename = filename.substring(1);
             }
             document.getElementById('csvfile_display').innerHTML = filename;
      }
    }
  </script>

 </body>
</html>
