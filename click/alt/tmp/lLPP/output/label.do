﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable 70 ""Bitte geben Sie Ihr Geschlecht an. "
label define 70 1 "weiblich" 2 "männlich" 3 "inter/divers" 4 "trans*"label variable 5 "etwas anderes:"


label values 70 70

label variable 120 ""Wo kam es zu der Gewalt durch die Polizei?  "
label variable 1201 "draußen in der Öffentlichkeit (z.B. Straße, Platz, Wald usw.) "
label variable 1202 "private Wohnung (inkl. Treppenhaus) oder privates Haus / Grundstück"
label variable 1203 "in einem öffentlichen Verkehrsmittel (z.B. Zug, Bus, U-Bahn, Straßenbahn usw.)"
label variable 1204 "Bahnhof oder Haltestelle eines öffentlichen Verkehrsmittels"
label variable 1205 "im Polizeifahrzeug (z.B. Streifenwagen, Einsatzfahrzeug, Transporter usw.) "
label variable 1206 "auf der Polizeiwache oder im Polizeigewahrsam (z.B. in der Zelle, Gefangenensammelstelle usw.) "
label variable 1207 "sonstiges öffentlich zugängliches Gebäude (z.B. Behörde, Kneipe, Stadion, Geschäft usw.)"
 99 "."
label values 120 120

label variable 530 "Sie haben angegeben, dass auch andere Polizist/innen anwesend waren, die keine Gewalt gegen Sie angewendet haben. Wie haben sich diese verhalten? Die anderen Polizist/innen haben…"
label variable 530scale "weggeguckt, die Tat ignoriert"
label variable 5301 "nicht eingegriffen und zugeschaut"
label variable 5302 "versucht zu schlichten"
label variable 5303 "verbal versucht den/die Täter/in abzuhalten"
label variable 5304 "körperlich versucht den/die Täter/in abzuhalten"
label variable 5305 "den/die Täter/in ermutigt"
label define 530 1 "trifft überhaupt nicht zu" 2 "trifft eher nicht zu" 3 "trifft teilweise zu " 4 "trifft eher zu" 5 "trifft voll und ganz zu" 98 "weiß ich nicht " 99 "weiter ohne Angabe"
label values 530* 530

label variable 102 "Wie häufig ist es in Ihrem Leben vorgekommen, dass Sie die gegen Sie eingesetzte Gewalt der Polizei als nicht notwendig oder übertrieben empfunden haben?"

label variable 84 "Der Fokus unserer Studie liegt auf persönlichen Erfahrungen mit körperlicher Gewalt durch die Polizei in Deutschland. Gibt es etwas, was Sie uns mitteilen möchten? Dann können Sie hier Anmerkungen oder Kritik hinterlassen."



exit