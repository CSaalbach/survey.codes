﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable v38 "Bitte geben Sie an, wieviel Entscheidungsfreiheit und Einfluss Sie nach Ihrem Empfinden darauf haben, wie Ihr weiteres Leben ablaeuft? "
label variable v38a "."
label define v38 1 "Ueberhaupt keine Freiheit" 2 "." 3 "." 4 "." 5 "." 6 "." 7 "." 8 "." 9 "." 10 "Voellige Freiheit" 88 "Weiss nicht" 99 "Keine Antwort"

exit