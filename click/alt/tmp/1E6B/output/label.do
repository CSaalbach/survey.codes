﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable sex "Welches Geschlecht haben Sie?"
 4 "männlich" 3 "weiblich" 2 "divers"label define sex 1 "keine Angabe"
label values sex sex

label variable age "Wie alt sind Sie?"

label variable studies "Welchen Abschluss streben Sie an?"
 5 "anderes" 4 "M. Sc." 3 "M. A." 2 "B. Sc."label define studies 1 "B. A."
label values studies studies

label variable job "Machen Sie ein Vollzeit- oder ein Teilzeitstudium?"
 2 "Teilzeit"label define job 1 "Vollzeit"
label values job job

label variable semester "In welchem Semester studieren Sie?"
 7 "7. Fachsemester +" 6 "6. Fachsemester" 5 "5. Fachsemester" 4 "4. Fachsemester" 3 "3. Fachsemester" 2 "2. Fachsemester"label define semester 1 "1. Fachsemester"
label values semester semester

label variable sport "Wie oft in der Woche treiben Sie Sport?"
 5 "0 Mal" 4 "1 bis 2 Mal" 3 "3 bis 4 Mal" 2 "5 bis 6 Mal"label define sport 1 "täglich"
label values sport sport

label variable tu "Nutzen Sie den Sportangebot der TU?"
 3 "Ich weiß nichts über den Angebot" 2 "Nein"label define tu 1 "Ja"
label values tu tu

label variable alc1 "Trinken Sie Alkohol?"
 2 "Nein"label define alc1 1 "Ja"
label values alc1 alc1

label variable alc2 "Wenn Ja, wie oft in der Woche?"
 5 "Unregelmäßig" 4 "1 bis 2 Mal" 3 "3 bis 4 Mal" 2 "5 bis 6 Mal"label define alc2 1 "täglich"
label values alc2 alc2

label variable smoking1 "Rauchen Sie?"
 2 "Nein"label define smoking1 1 "Ja"
label values smoking1 smoking1

label variable smoking2 "Wenn ja, wie viele Zigaretten pro Tag?"
 4 "mehr als 15" 3 "10 bis 15" 2 "6 bis 10"label define smoking2 1 "1 bis 5"
label values smoking2 smoking2

label variable sleep "Wie viele Stunden schlafen Sie pro Tag?"

label variable habits "Für wie nützlich halten Sie die folgenden Tätigkeiten?"
label variable habitsscale "Joggen"
label variable habits1 "Meditieren"
label variable habits2 "Schlafen"
label variable habits3 "Schwimmen"
label variable habits4 "Gesunde Ernährung"
 5 "sehr nützlich" 4 "eher nützlich" 3 "teils, teils" 2 "eher unnützlich"label define habits 1 "sehr unnützlich"
label values habits* habits

label variable time "Denken Sie, dass Sie trotz Ihres Studiums genug Zeit für Gesundheit haben?"
 4 "ich habe nur in der vorlesungsfreien Zeit Zeit für Gesundheit" 3 "ich habe nie Zeit für meine Gesundheit" 2 "ich habe wenig Zeit für meine Gesundheit"label define time 1 "ich habe fast immer Zeit für meine Gesundheit"

exit