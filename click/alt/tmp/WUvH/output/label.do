﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable krankenhaus "Gab es bei Ihrem Kind in den letzten \underline{12 Monaten} gesundheitliche Probleme, die einen \underline{Krankenhausaufenthalt} notwendig machten?"
 2 "Nein"label define krankenhaus 1 "Ja"
label values krankenhaus krankenhaus

label variable erkrankungen ""Ist im Rahmen einer \""{a}rztlichen Untersuchung bei Ihrem Kind einmal eine der folgenden Erkrankungen oder St\""{o}rungen festgestellt worden?""
label variable erkrankungen1 "Asthma"
label variable erkrankungen2 "Chronische Bronchitis"
label variable erkrankungen3 "Heuschnupfen"
label define erkrankungen 1 "."
label values erkrankungen erkrankungen

label variable befinden "Wie sehen Sie Ihr Kind heute?"
label variable befinden1 "Mein Kind ist meist fr\”{o}hlich und zufrieden"
label variable befinden2 "Mein Kind ist schwer zu tr\”{o}sten"
label variable befinden3 "Mein Kind ist neugierig und aktiv"
label define befinden 1 "Trifft voll zu" 2 "Trifft eher zu" 3 "Trifft eher nicht zu" 4 "Trifft gar nicht zu"

exit