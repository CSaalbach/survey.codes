﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable v225 "Sind Sie ein Mann oder eine Frau?"
 3 "Eine Frau" 2 "Ein Mann"label define v225 1 "Keine Antwort"
label values v225 v225

label variable v226 "Bitten geben Sie Ihr Geburtsjahr an"

label variable v227 "Wurden Sie in Deutschland geboren?"
 4 "Ja" 3 "Nein" 2 "Weiss nicht"label define v227 1 "Keine Antwort"
label values v227 v227

label variable v169 "Haben Sie die deutsche Staatsbürgerschaft?"
 4 "Ja" 3 "Nein" 2 "Weiss nicht"label define v169 1 "Keine Antwort"
label values v169 v169

label variable v170 "Wie stolz sind Sie darauf, dass Sie die deutsche Staatsburgerschaft haben? Sind Sie..."
 6 "Sehr stolz" 5 "Ziemlich stolz" 4 "Nicht sehr stolz" 3 "Uberhaupt nicht stolz" 2 "Weiss nicht"label define v170 1 "Keine Antwort"
label values v170 v170

label variable v174 "Welcher politischen Partei stehen Sie am nachsten? "
 8 "CDU CSU" 7 "SPD" 6 "FDP" 5 "Bundnis 90 Die Grunen" 4 "Die Linke" 3 "AFD" 2 "Weiss nicht"label define v174 1 "Keine Antwort"
label values v174 v174

label variable v174a "Einer anderen Partei bitte hier eintragen"

label variable section "Bitte sagen Sie mir fuer jeden Bereich, ob er Ihnen sehr wichtig, ziemlich wichtig, nicht wichtig oder uberhaupt nicht wichtig ist."
label variable sectionscale "Arbeit"
label variable section1 "Familie"
label variable section2 "Freunde und Bekannte"
label variable section3 "Freizeit"
label variable section4 "Politik"
label variable section5 "Religion"
label define section 1 "Sehr wichtig " 2 "Ziemlich wichtig  " 3 "Nicht wichtig  " 4 "Ueberhaupt nicht wichtig  " 5 "Weiss nicht" 6 "Keine Antwort"
label values section* section

label variable v7 "Ganz allgemein wuerden Sie sagen Sie sind zurzeit"
 6 "Sehr gluecklich" 5 "Ziemlich gluecklich" 4 "Nicht sehr gluecklich" 3 "Ueberhaupt nicht gluecklich" 2 "Weiss nicht"label define v7 1 "Keine Antwort"
label values v7 v7

label variable v8 "Wie schaetzen Sie insgesamt Ihren Gesundheitszustand ein?"
 7 "Sehr gut" 6 "Gut" 5 "Durchschnittlich" 4 "Schlecht" 3 "Sehr schlecht" 2 "Weiss nicht"label define v8 1 "Keine Antwort"
label values v8 v8

label variable v21 "Waren Sie in den vergangenen 6 Monaten ehrenamtlich taetig"
 4 "Ja" 3 "Nein" 2 "Weiss nicht"label define v21 1 "Keine Antwort"
label values v21 v21

label variable v31 "Wuerden Sie ganz allgemein sagen, dass man den meisten Menschen vertrauen kann, oder dass man im Umgang mit Menschen nicht vorsichtig genug sein kann? "
 4 "Man kann den meisten vertrauen " 3 "Man kann nicht vorsichtig genug sein " 2 "Weiss nicht"label define v31 1 "Keine Antwort"
label values v31 v31

label variable life "Ich moechte Ihnen jetzt verschiedene Bereiche vorlesen und Sie fragen, wie wichtig diese in Ihrem Leben sind. Bitte sagen Sie mir fuer jeden Bereich, ob er Ihnen sehr wichtig, ziemlich wichtig, nicht wichtig oder ueberhaupt nicht wichtig ist."
label variable lifescale "Ihre Familie"
label variable life1 "Menschen in Ihrer Nachbarschaft "
label variable life2 "Menschen, die Sie persoenlich kennen "
label variable life3 "Menschen, denen Sie zum ersten Mal begegnen "
label variable life4 "Menschen anderer Religion "
label variable life5 "Menschen anderer Nationalitaet "
label define life 1 "Vertraue voellig" 2 "Vertraue ziemlich" 3 "Vertraue kaum" 4 "Vertraue gar nicht" 5 "Weiss nicht" 6 "Keine Antwort"
label values life* life

label variable v38 "Bitte geben Sie an, wieviel Entscheidungsfreiheit und Einfluss Sie nach Ihrem Empfinden darauf haben, wie Ihr weiteres Leben ablaeuft? "
 12 "1 = Ueberhaupt keine Freiheit" 11 "2" 10 "3" 9 "4" 8 "5" 7 "6" 6 "7" 5 "8" 4 "9" 3 "10 = Voellige Freiheit" 2 "Weiss nicht"label define v38 1 "Keine Antwort"
label values v38 v38

label variable v39 "Wenn Sie einmal alles beruecksichtigen, wie zufrieden sind Sie insgesamt zurzeit mit Ihrem Leben?"
 12 "1 = Ueberhaupt nicht zufrieden" 11 "2" 10 "3" 9 "4" 8 "5" 7 "6" 6 "7" 5 "8" 4 "9" 3 "10 = Voellig zufrieden" 2 "Weiss nicht"label define v39 1 "Keine Antwort"
label values v39 v39

label variable work "Hier steht Verschiedenes ueber die berufliche Arbeit. Bitte sagen Sie mir, was davon Sie persoenlich an einem Beruf fuer ganz besonders wichtig halten"
label variable work1 "Gute Bezahlung"
label variable work2 "Angenehme Arbeitszeiten"
label variable work3 "Die Moeglichkeit eigene Initiative zu entfalten"
label variable work4 "Grosszuegige Urlaubsreglungen"
label variable work5 "Ein Beruf bei dem man das Gefuehl hat etwas erreichen zu koennen"
label variable work6 "Ein Beruf mit Verantwortung"
label define work 1 "."
label values work work

label variable v51 "Gehoeren Sie einer Religionsgemeinschaft an?"
 4 "Ja (Weiter mit Frage 14)" 3 "Nein" 2 "Weiss nicht  (Weiter mit Frage 14)"label define v51 1 "Keine Antwort (Weiter mit Frage 14)"
label values v51 v51

label variable v52 "Welcher Religionsgemeinschaft gehoeren Sie an?"
 8 "Der roemisch katholischen Kirche" 7 "Der evangelischen Kirche" 6 "Einer evangelischen Freikirche" 5 "Der griechisch orthodoxen Kirche" 4 "Der russisch orthodoxen Kirche" 3 "Dem Islam" 2 "Weiss nicht"label define v52 1 "Keine Antwort"
label values v52 v52

label variable v52a "Einer anderen Religionsgemeinschaft, und zwar bitte hier eintragen"

label variable v54 "Haben Sie jemals einer Religionsgemeinschaft angehoert?"
 4 "Ja" 3 "Nein" 2 "Weiss nicht"label define v54 1 "Keine Antwort"
label values v54 v54

label variable skills "Welche dieser Eigenschaften von Kindern sind nach Ihrer Meinung am sinnvollsten? "
label variable skills1 "Gutes Benehmen"
label variable skills2 "Unabhaengigkeit Selbststaendigkeit"
label variable skills3 "Fleiss"
label variable skills4 "Verantwortungsgefuehl"
label variable skills5 "Vorstellungskraft Fantasie"
label variable skills6 "Toleranz und Respekt gegenueber Mitmenschen"
label variable skills7 "Sparsamkeit im Umgang mit Geld und Dingen"
label variable skills8 "Entschlossenheit, Ausdauer"
label variable skills9 "Religioeser Glaube"
label variable skills10 "Selbstlosigkeit"
label variable skills11 "Gehorsam"
label define skills 1 "."

exit