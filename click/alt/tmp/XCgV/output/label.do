﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable self "Bitte bewerten sie die folgenden Aussagen"
label variable selfscale "Ich kann wichtige Begriffe, Sachverhalte aus dieser Lehrveranstaltung wiedergeben"
label variable self1 "Ich sehe mich nun in der Lage, eine typische Fragestellung des Themengebietes dieser Lehrveranstaltung zu bearbeiten"
label variable self2 "Ich kann die Sachverhalte aus dieser Lehrveranstaltung in der praktischen Arbeit anwenden"
label define self 1 "trifft völlig zu" 2 "trifft eher zu" 3 "teils, teils" 4 "trifft eher nicht zu" 5 "trifft gar nicht zu"
label values self* self

label variable bal "Der Anteil von"
label variable balscale "inhaltlichen Erklärungen"
label variable bal1 "Kleingruppen – Aktivitäten war"
label variable bal2 "Austausch in der Gesamtgruppe war"
label define bal 1 "sehr hoch" 2 "eher hoch" 4 "eher niedrig" 5 "sehr niedrig"
label values bal* bal

label variable atmo "Im Seminar"
label variable atmoscale "herrschte ein respektvoller Umgang miteinander"
label variable atmo1 "habe ich mich aktiv beteiligt (Fragen, Kommentare, Diskussionen)"
label variable atmo2 "hatte ich genügend Zeit, über meine Schwierigkeiten mit den Lerninhalten zu diskutieren"
label define atmo 1 "immer" 2 "oft" 3 "gelegentlich" 4 "selten" 5 "nie"
label values atmo* atmo

label variable mat "Wie hilfreich waren folgende Materialien für Ihr Verständnis des Lehrstoffs?"
label variable matscale "die eingesetzten Medien zur Präsentation der Lerninhalte"
label variable mat1 "die bereitgestellten Aufgaben/Übungen"
label variable mat2 "die bereitgestellten Skripte, Texte"
label variable mat3 "die empfohlene Literatur"
label define mat 1 "sehr hilfreich" 2 "eher hilfreich" 3 "teils, teils" 4 "eher nicht hilfreich" 5 "gar nicht hilfreich" 6 "keine Bereitstellung"
label values mat* mat

label variable insight "Das nehme ich aus dem Seminar mit"



exit