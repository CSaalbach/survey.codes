﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable krankenhaus "Gab es bei Ihrem Kind in den letzten 12 Monaten gesundheitliche Probleme, die einen Krankenhausaufenthalt notwendig machten?"
 2 ""label define krankenhaus 1 ""
label values krankenhaus krankenhaus

label variable erkrankungen "Ist im Rahmen einer aerztlichen Untersuchung bei Ihrem Kind einmal eine der folgenden Erkrankungen oder Stoerungen festgestellt worden?"
label variable erkrankungen1 "Asthma"
label variable erkrankungen2 "Chronische Bronchitis"
label variable erkrankungen3 "Heuschnupfen"
label variable erkrankungen4 "Schwerhoerigkeit"
label define erkrankungen 1 ""
label values erkrankungen erkrankungen

label variable befinden "Wie sehen Sie Ihr Kind heute?"
label variable befinden1 "Mein Kind ist meist froehlich und zufrieden"
label variable befinden2 "Mein Kind ist schwer zu troesten"
label variable befinden3 "Mein Kind ist neugierig und aktiv"
 Trifft voll zu
 "" Trifft eher zu
 "" Trifft eher nich zu
 "" Trifft gar nicht zu
 ""

exit