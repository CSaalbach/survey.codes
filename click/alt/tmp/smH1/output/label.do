﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable shop "Does the shop in which you work belong to a capitalist or to a limited company? "
 3 "capitalist" 2 "limited company"label define shop 1 "don‘t know"
label values shop shop

label variable childage "What is the youngest age at which children are taken on?"
 7 "younger than 13 years" 6 "13 years" 5 "14 years" 4 "15 years" 3 "16 years" 2 "older than 16 years"label define childage 1 "don‘t know"
label values childage childage

label variable shoplocal "Is the shop in a village, or in a town? "
 2 "village"label define shoplocal 1 "town"
label values shoplocal shoplocal

label variable agrilabor "If your shop is in the country, is there sufficient work in the factory for your existence, or are you obliged to combine it with agricultural labor?"
 2 "sufficient work in the factory"label define agrilabor 1 "combine it with agricultural labor"
label values agrilabor agrilabor

label variable safety "Are safety measures to prevent accidents applied to the engine, transmission and machinery"
label variable safety1 "engine"
label variable safety2 "transmission"
label variable safety3 "machinery"
label define safety 1 "."
label values safety safety

label variable hyg "How satisfied are you with the hygienic conditions in the workshop regarding the following aspects."
label variable hygscale "size of the rooms space"
label variable hyg1 "ventilation"
label variable hyg2 "temperature"
label variable hyg3 "plastering"
label variable hyg4 "lavatories"
label variable hyg5 "general cleanliness"
label variable hyg6 "noise of machinery"
label variable hyg7 "metallic dust"
label variable hyg8 "dampness"
label define hyg 1 "very satisfied" 2 "somewhat satisfied" 3 "somewhat dissatisfied" 4 "very dissatisfied" 5 "does not apply"
label values hyg* hyg

label variable accidents "Mention the accidents which have taken place to your personal knowledge."

label variable holidays "State the number of holidays in the course of a year."



exit