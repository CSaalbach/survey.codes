﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable shop "Gehört das Unternehmen, in dem Sie arbeiten, Privatkapitalisten oder einer Aktiengesellschaft?"
 3 "Privatkapitalisten" 2 "Aktiengesellschaft"label define shop 1 "weiß nicht"
label values shop shop

label variable childage "Was ist das Mindestalter, zu dem Kinder eingestellt werden?"
 7 "jünger als 13 Jahre" 6 "13 Jahre" 5 "14 Jahre" 4 "15 Jahre" 3 "16 Jahre" 2 "älter als 16 Jahre"label define childage 1 "weiß nicht"
label values childage childage

label variable shoplocal "Liegt die Arbeitsstätte auf dem Lande oder in der Stadt?"
 2 "auf dem Lande"label define shoplocal 1 "in der Stadt"
label values shoplocal shoplocal

label variable agrilabor "Falls Ihr Gewerbe auf dem Lande betrieben wird: bildet es Ihre hauptsächliche Erwerbsquelle oder betreiben Sie es zusätzlich zu oder gemeinsam mit der Landwirtschaft?"
 2 "hauptsächliche Erwerbsquelle"label define agrilabor 1 "zusätzlich zu der Landwirtschaft"
label values agrilabor agrilabor

label variable safety "Sind die Antriebskraft, die Transmissionsvorrichtungen und die laufenden Maschinen mit ausreichenden Schutzvorrichtungen gegen Unfälle versehen?"
label variable safety1 "Antriebskraft"
label variable safety2 "Transmissionsvorrichtungen"
label variable safety3 "Maschinen"
label define safety 1 "."
label values safety safety

label variable hyg "Wie sehen Sie Ihr Kind heute?"
label variable hygscale "Mein Kind ist zufrieden"
label variable hyg1 "die Lüftung"
label variable hyg2 "die Temperatur"
label variable hyg3 "ob die Wände geweißt sind"
label variable hyg4 "Abortverhältnisse"
label variable hyg5 "allgemeine Reinlichkeit"
label variable hyg6 "Maschinenlärm"
label variable hyg7 "Staub"
label variable hyg8 "Feuchtigkeit"
label define hyg 1 "sehr zufrieden" 2 "eher zufrieden" 3 "eher unzufrieden" 4 "sehr unzufrieden" 5 "trifft nicht zu"
label values hyg* hyg

label variable accidents "Berichten Sie aus eigener Erfahrung von Unfällen, die Verletzungen bzw. den Tod von Arbeitern verursachten."

label variable holidays "Wieviel Feiertage haben Sie während des Jahres?"



exit