﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable sex "Welches Geschlecht haben Sie?"
label define sex 1 "weiblich" 2 "männlich" 3 "divers" 99 "keine Angabe"
label values sex sex

label variable age "Wie alt sind Sie?"

label variable sport "Wie oft in der Woche machen Sie Sport?"
label define sport 1 "0 Mal" 2 "1 bis 2 Mal" 3 "3 bis 4 Mal" 4 "5 bis 6 Mal" 5 "täglich"
label values sport sport

label variable alc1 "Trinken Sie Alkohol?"
label define alc1 1 "Ja" 2 "Nein"
label values alc1 alc1

label variable alc2 "Wenn ja, wie oft in der Woche?"
label define alc2 1 "unregelmäßig" 2 "1 bis 2 Mal" 3 "3 bis 4 Mal" 4 "5 bis 6 Mal" 5 "täglich"
label values alc2 alc2

label variable smoking1 "Rauchen Sie?"
label define smoking1 1 "Ja" 2 "Nein"
label values smoking1 smoking1

label variable smoking2 "Wenn ja, wie oft in der Woche?"
label define smoking2 1 "unregelmäßig" 2 "1 bis 2 Mal" 3 "3 bis 4 Mal" 4 "5 bis 6 Mal" 5 "täglich"
label values smoking2 smoking2

label variable sleep "Wieviele Stunden schlafen Sie am Tag?"

label variable habits "Für wie nützlich halten Sie die folgenden Dinge zur Förderung der Gesundheit?"
label variable habits1 "Joggen"
label variable habits2 "Meditieren"
label variable habits3 "Schlafen"
label variable habits4 "Schwimmen"
label variable habits5 "Gesunde Ernährung"
label define habits 1 "sehr nützlich" 2 "eher nützlich" 3 "teils, teils" 4 "eher unnützlich" 5 "sehr unnützlich"

exit