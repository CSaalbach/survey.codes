﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable Geschlecht "Welches Geschlecht haben Sie?"
 3 "männlich" 2 "weiblich"label define Geschlecht 1 "anderes"
label values Geschlecht Geschlecht

label variable Bildung "Was ist Ihr höchster Bildungsabschluss?"
 6 "Haupt- oder Volksschulabschluss" 5 "Mittlere Reife oder Abschluss der polytechnischen Oberschule" 4 "Abitur, Fachhochschulreife (Gymnasium oder erweiterte Oberschule EOS)" 3 "Universitäts-, Hochschul- bzw. Fachhochschulabschluss" 2 "Promotion"label define Bildung 1 "ohne Schulabschluss"

exit