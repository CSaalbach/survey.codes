﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable sex-children "Ist Ihr Kind ein Junge oder ein Mädchen?"
 2 "Mädchen "label define sex-children 1 "Junge"
label values sex-children sex-children

label variable care-children "Wie wird Ihr Kind normalerweise - außer von Ihnen und/oder dem Zweiten Elternteil - betreut? "
label variable care-children1 "Von den Großeltern"
label variable care-children2 "Von den Geschwistern"
label variable care-children3 "Von einem bezahlten Helfer (z.B. Kindermädchen, Au-pair)"
label variable care-children4 "Von einem unbezahlten Helfer (z.B. Nachbarn und Freunde)"
label variable care-children5 "In einer Kindertageseinrichtung (z.B. Krippe, Kindergarten, Kindertagesstätte)"
label variable care-children6 "Von einer Tagesmutter bzw. einem Tagesvater (Tagespflege)"
label variable care-children7 "Ausschließlich von Ihnen bzw. dem zweiten Elternteil"
label define care-children 1 "."
label values care-children care-children

label variable daycare-choic ""Wie wichtig waren die folgenden Punkte für Sie bei der Wahl der Kindertagesbetreuung? "
label variable daycare-choicscale "Die Öffnungszeiten"
label variable daycare-choic1 "Die Nähe zum Wohnort"
label variable daycare-choic2 "Die Nähe zum Arbeitsplatz des Vaters"
label variable daycare-choic3 "Die Nähe zum Arbeitsplatz der Mutter"
label variable daycare-choic4 "Ein gute soziale Durchmischung der Gruppe"
label variable daycare-choic5 "Aufgeschlossenheit für andere Kulturen"
label variable daycare-choic6 "Die Kosten"
label variable daycare-choic7 "Eine kleine Gruppe"
label variable daycare-choic8 "Gute, individuelle Förderangebote für Kinder mit besonderen Förderbedarfen"
label variable daycare-choic9 "Ein gesundes und frisch gekochtes Essen"
label variable daycare-choic10 "Ein gute Ausstattung und gute Räumlichkeiten"
label define daycare-choic 1 "sehr wichtig (1)" 2 "2" 3 "3" 4 "4" 5 "5" 6 "überhaupt nicht wichtig (6)" 7 "trifft nicht zu"
label values daycare-choic* daycare-choic

label variable daycare-hours "Wie viele Stunden pro Woche besucht Ihr Kind normalerweise die Kindertagesbetreuung?"

label variable job "Welche berufliche Tätigkeit übt Ihr Partner bzw. Ihre Partnerin gegenwärtig aus? "



exit