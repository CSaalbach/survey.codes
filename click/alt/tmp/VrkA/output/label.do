﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable e1k1 "Ich nehme an der Verlosung des iPad teil."
 2 "Ja"label define e1k1 1 "Nein"
label values e1k1 e1k1

label variable e2k1V "Früher hatten Menschen häufig geringere Bildungschancen als heute. Bitte nennen Sie uns den höchsten Schulabschluss Ihres Vaters."
 5 "Volksschul-, Hauptschulabschluss" 4 "Mittlere Reife, 10. Klasse, Realschulabschluss, Fachschulreife" 3 "Fachhochschulreife, fachgebundene Hochschulreife, Abschluss einer Fachoberschule" 2 "Abitur, allgemeine Hochschulreife"label define e2k1V 1 "Von der Schule abgegangen ohne Abschluss." 0 "Anderen Schulabschluss, bitte notieren:"
label values e2k1V e2k1V

label variable e2k1VS "Anderen Schulabschluss, bitte notieren:"

label variable e2k4VN "Wann wurde Ihr Vater geboren?"

label variable e2k7V "Mit wem lebt Ihr Vater zusammen?"
label variable e2k7V1 "Gemeinsam mit Ihrer Mutter in einem Haushalt."
label variable e2k7V2 "Mit einer anderen Partnerin in einem Haushalt."
label variable e2k7V3 "Mit jemand anderem in einem Haushalt."
label variable e2k7V4 "Allein."
label variable e2k7V5 "In einem Alters- oder Pflegeheim, in betreuter Wohnanlage."
label variable e2k7V6 "Sonstiges."
label variable e2k7V7 "Kann ich nicht beantworten."
label define e2k7V 1 "."
label values e2k7V e2k7V

label variable e2k12Q1 "Mein Vater bzw. meine Mutter sind für mich wie gute Freunde."
label variable e2k12Q1scale "Vater"
label variable e2k12Q11 "Mutter"
label define e2k12Q1 1 "Trifft genau zu" 2 "Trifft eher zu" 3 "Trifft eher nicht zu" 4 "Trifft gar nicht zu"
label values e2k12Q1* e2k12Q1

label variable e2k12Q2 "Mein Vater bzw. meine Mutter mag mich so, wie ich bin."
label variable e2k12Q2scale "Vater"
label variable e2k12Q21 "Mutter"
label define e2k12Q2 1 "Trifft genau zu" 2 "Trifft eher zu" 3 "Trifft eher nicht zu" 4 "Trifft gar nicht zu"
label values e2k12Q2* e2k12Q2

label variable e2k12Q3 "Ich kann meinem Vater bzw. meiner Mutter alles erzählen, was mich beschäftigt."
label variable e2k12Q3scale "Vater"
label variable e2k12Q31 "Mutter"
label define e2k12Q3 1 "Trifft genau zu" 2 "Trifft eher zu" 3 "Trifft eher nicht zu" 4 "Trifft gar nicht zu"

exit