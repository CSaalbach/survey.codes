﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable leistung "Sozialleistungen in Deutschland…"
label variable leistungscale "belasten die Volkswirtschaft zu stark."
label variable leistung1 "verhindern weit verbreitete Armut."
label variable leistung2 "führen zu mehr Gleichheit in der Gesellschaft."
label variable leistung3 "ermutigen Menschen aus anderen Ländern, hierher zu kommen, um hier zu leben."
label variable leistung4 "kosten die Unternehmen zu hohe Steuern und Abgaben."
label variable leistung5 "machen es einfacher, Beruf und Familie zu vereinbaren."
label define leistung 1 "Stimme stark zu" 2 "Stimme zu" 3 "Weder noch" 4 "Lehne ab" 5 "Lehne stark ab"
label values leistung* leistung

label variable gender "Welches Geschlecht haben Sie?"
 3 "weiblich" 2 "männlich"label define gender 1 "anders"
label values gender gender

label variable zustimm "Und wie stark stimmen Sie dem zu oder lehnen es ab, dass Sozialleistungen in Deutschland…"
label variable zustimmscale "die Menschen faul machen?"
label variable zustimm1 "dazu beitragen, dass Menschen weniger dazu bereit sind, sich umeinander zu kuemmern?"
label variable zustimm2 "dazu beitragen, dass Menschen weniger dazu bereit sind, sich um sich selbst und um ihre Familie zu kuemmern?"
label define zustimm 1 "Stimme stark zu" 2 "Stimme zu" 3 "Weder noch" 4 "Lehne ab" 5 "Lehne stark ab"
label values zustimm* zustimm

label variable stand "Bitte sagen Sie mir, wie schätzen Sie…"
label variable standscale "den Lebensstandard von Rentnern und Pensionären im Großen und Ganzen ein?"
label variable stand1 "den Lebensstandard von Arbeitslosen im Großen und Ganzen ein?"
label variable stand2 "das Angebot bezahlbarer Kinderbetreuungsmöglichkeiten für berufstätige Eltern im Großen und Ganzen ein?"
label variable stand3 "im Großen und Ganzen die Chancen von jungen Menschen ein, zum ersten Mal eine Stelle zu finden?"
label define stand 1 "äußerst gut" 2 "." 3 "." 4 "." 5 "." 6 "." 7 "." 8 "." 9 "." 10 "." 11 "äußerst schlecht"

exit