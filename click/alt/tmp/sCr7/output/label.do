﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable e1k1 "Ich nehme an der Verlosung des iPad teil."
 2 "Ja"label define e1k1 1 "Nein"
label values e1k1 e1k1

label variable e2k1V "Früher hatten Menschen häufig geringere Bildungschancen als heute. Bitte nennen Sie uns den höchsten Schulabschluss Ihres Vaters und ihrer Ihrer Mutter."
label variable e2k1Vscale "Volksschul-, Hauptschulabschluss"
label variable e2k1V1 "Mittlere Reife, 10. Klasse, Realschulabschluss, Fachschulreife"
label variable e2k1V2 "Fachhochschulreife, fachgebundene Hochschulreife, Abschluss einer Fachoberschule"
label variable e2k1V3 "Abitur, allgemeine Hochschulreife"
label variable e2k1V4 "Von der Schule abgegangen ohne Abschluss."
label define e2k1V 1 "Vater" 2 "Mutter"
label values e2k1V* e2k1V

label variable e2k1VV "Anderen Schulabschluss des Vaters, bitte notieren:"

label variable e2k1VM "Anderen Schulabschluss der Mutter, bitte notieren:"

label variable e2k2V "Bitten nennen Sie uns den höchsten beruflichen Ausbildungsabschluss Ihres Vaters und Ihrer Mutter."
label variable e2k2Vscale "Keine berufliche Ausbildung (auch Berufsschule ohne Lehre)"
label variable e2k2V1 "Berufsschule mit gewerblicher oder landwirtschaftlicher Lehre"
label variable e2k2V2 "Berufsschule mit kaufmännischer oder sonstiger Lehre"
label variable e2k2V3 "Berufsfachschule"
label variable e2k2V4 "Meister-, Techniker- oder gleichwertige Fachschule"
label variable e2k2V5 "Fachhochschule (auch Ingenieurschule)"
label variable e2k2V6 "Hochschulabschluss"
label define e2k2V 1 "Vater" 2 "Mutter"
label values e2k2V* e2k2V

label variable e2k2VV "Andere berufliche Ausbildung des Vaters, bitte notieren:"

label variable e2k2VM "Andere berufliche Ausbildung der Mutter, bitte notieren:"

label variable e2k3VS "Bitte erinnern Sie sich: Welche Tätigkeit übten Ihr Vater und Ihre Mutter in ihrem Hauptberuf aus, als Sie selbst 16 Jahre alt waren?"

label variable e2k3MS "."

label variable e2k4VN "Wann wurde Ihr Vater geboren?"

label variable e2k4MN "Wann wurde Ihre Mutter geboren?"

label variable e2k5VS "In welchem Land wurden Ihr Vater geboren?"

label variable e2k5MS "In welchem Land wurde Ihre Mutter geboren?"

label variable e2k6V "Ist Ihr Vater noch am Leben?"
 2 "Ja"label define e2k6V 1 "Nein"
label values e2k6V e2k6V

label variable e2k6VN "Wann ist Ihr Vater verstorben?"

label variable e2k6M "Ist Ihre Mutter noch am Leben?"
 2 "Ja"label define e2k6M 1 "Nein"
label values e2k6M e2k6M

label variable e2k6MN "Wann ist Ihre Mutter verstorben?"

label variable e2k7V "Mit wem lebt Ihr Vater bzw. Ihre Mutter zusammen?"
label variable e2k7Vscale "Gemeinsam mit Ihrer Mutter bzw. Vater in einem Haushalt."
label variable e2k7V1 "Mit einer anderen Partnerin bzw. einem anderen Partner in einem Haushalt."
label variable e2k7V2 "Mit jemand anderem in einem Haushalt."
label variable e2k7V3 "Allein."
label variable e2k7V4 "In einem Alters- oder Pflegeheim, in betreuter Wohnanlage."
label variable e2k7V5 "Sonstiges."
label variable e2k7V6 "Kann ich nicht beantworten."
label define e2k7V 1 "Vater" 2 "Mutter"
label values e2k7V* e2k7V

label variable e2k8V "Wie geht es Ihrem Vater und Ihrer Mutter zurzeit gesundheitlich, insgesamt gesehen?"
label variable e2k8Vscale "Sehr gut"
label variable e2k8V1 "Gut"
label variable e2k8V2 "Es geht so (mittelmäßig)."
label variable e2k8V3 "Schlecht"
label variable e2k8V4 "Sehr schlecht"
label variable e2k8V5 "Kann ich nicht beantworten"
label define e2k8V 1 "Vater" 2 "Mutter"
label values e2k8V* e2k8V

label variable e2k9V "Benötigen Ihr Vater oder Ihre Mutter innerhalb der letzten 12 Monate regelmäßige Hilfe bei täglichen Verrichtungen, wie z.B. essen, aufstehen, anziehen, baden oder zur Toilette gehen?"
label variable e2k9Vscale "Ja"
label variable e2k9V1 "Nein"
label variable e2k9V2 "Kann ich nicht beantworten"
label define e2k9V 1 "Vater" 2 "Mutter"
label values e2k9V* e2k9V

label variable e2k10V "Wie oft haben Sie normalerweise Kontakt mit Ihrem Vater und Ihrer Mutter, egal ob persönlich, telefonisch oder schriftlich?"
label variable e2k10Vscale "Täglich"
label variable e2k10V1 "Mehrmals in der Woche"
label variable e2k10V2 "Einmal in der Woche"
label variable e2k10V3 "1-3 Mal im Monat"
label variable e2k10V4 "Mehrmals im Jahr"
label variable e2k10V5 "Seltener"
label variable e2k10V6 "Kein Kontakt"
label variable e2k10V7 "Kann ich nicht beantworten."
label define e2k10V 1 "Vater" 2 "Mutter"
label values e2k10V* e2k10V

label variable e2k11V "Wie lange brauchen Sie, um zu Ihrem Vater und zu Ihrer Mutter zu kommen (an einem gewöhnlichen Tag, mit einem Verkehrsmittel, das Sie normalerweise benutzen)?"
label variable e2k11Vscale "Wir wohnen in einem Haus."
label variable e2k11V1 "Weniger als 10 Minuten"
label variable e2k11V2 "10 bis weniger als 30 Minuten"
label variable e2k11V3 "30 Minuten bis weniger als 1 Stunde"
label variable e2k11V4 "1 Stunde bis weniger als 3 Stunden"
label variable e2k11V5 "3 Stunden und mehr"
label variable e2k11V6 "Kann ich nicht beantworten."
label define e2k11V 1 "Vater" 2 "Mutter"
label values e2k11V* e2k11V

label variable e2k12A "Mein Vater bzw. meine Mutter sind für mich wie gute Freunde."
label variable e2k12Ascale "Vater"
label variable e2k12A1 "Mutter"
label define e2k12A 1 "Trifft genau zu" 2 "Trifft eher zu" 3 "Trifft eher nicht zu" 4 "Trifft gar nicht zu"
label values e2k12A* e2k12A

label variable e2k12B "Mein Vater bzw. meine Mutter mag mich so, wie ich bin."
label variable e2k12Bscale "Vater"
label variable e2k12B1 "Mutter"
label define e2k12B 1 "Trifft genau zu" 2 "Trifft eher zu" 3 "Trifft eher nicht zu" 4 "Trifft gar nicht zu"
label values e2k12B* e2k12B

label variable e2k12C "Ich kann meinem Vater bzw. meiner Mutter alles erzählen, was mich beschäftigt."
label variable e2k12Cscale "Vater"
label variable e2k12C1 "Mutter"
label define e2k12C 1 "Trifft genau zu" 2 "Trifft eher zu" 3 "Trifft eher nicht zu" 4 "Trifft gar nicht zu"
label values e2k12C* e2k12C

label variable e2k12D "Ich genieße das Zusammensein mit meinem Vater bzw. meiner Mutter."
label variable e2k12Dscale "Vater"
label variable e2k12D1 "Mutter"
label define e2k12D 1 "Trifft genau zu" 2 "Trifft eher zu" 3 "Trifft eher nicht zu" 4 "Trifft gar nicht zu"
label values e2k12D* e2k12D

label variable e2k12E "Obwohl ich meinen Vater bzw. meine Mutter liebe, bin ich häufig auch ärgerlich auf ihn/sie."
label variable e2k12Escale "Vater"
label variable e2k12E1 "Mutter"
label define e2k12E 1 "Trifft genau zu" 2 "Trifft eher zu" 3 "Trifft eher nicht zu" 4 "Trifft gar nicht zu"
label values e2k12E* e2k12E

label variable e2k12F "Mein Vater bzw. meine Mutter und ich geraten oft aneinander, aber trotzdem ist die Beziehung eng."
label variable e2k12Fscale "Vater"
label variable e2k12F1 "Mutter"
label define e2k12F 1 "Trifft genau zu" 2 "Trifft eher zu" 3 "Trifft eher nicht zu" 4 "Trifft gar nicht zu"
label values e2k12F* e2k12F

label variable e3k13A "Wie oft passiert es zwischen Ihnen und Ihrem Vater bzw. zwischen Ihnen und Ihrer Mutter, dass sie ärgerlich oder wütend aufeinander sind?"
label variable e2k13Ascale "Vater & ich"
label variable e2k13A1 "Mutter & ich"
label define e3k13A 1 "Immer" 2 "Häufig" 3 "Manchmal" 4 "Seltener" 5 "Nie"
label values e3k13A* e3k13A

label variable e2k13B "Wie oft sind Ihr Vater bzw. Ihre Mutter und Sie unterschiedlicher Meinung und streiten sich?"
label variable e2k13Bscale "Vater & ich"
label variable e2k13B1 "Mutter & ich"
label define e2k13B 1 "Immer" 2 "Häufig" 3 "Manchmal" 4 "Seltener" 5 "Nie"
label values e2k13B* e2k13B

label variable e2k14A "Wie häufig werden Sie von Ihrem Vater und Ihrer Mutter unterstützt? Unterstützung bei alltäglichen Arbeiten (z.B. Arbeiten im Haushalt, Besorgungen machen, Kinderbetreuung)."
label variable e2k14Ascale "Vater unterstützt mich."
label variable e2k14A1 "Mutter unterstützt mich."
label define e2k14A 1 "Täglich" 2 "Mehrmals in der Woche" 3 "Mehrmals im Monat" 4 "Ein paar Mal im Jahr" 5 "Nie"
label values e2k14A* e2k14A

label variable e2k15A "Wie häufig unterstützen Sie umgekehrt Ihren Vater und Ihre Mutter? Unterstützung bei alltäglichen Arbeiten (z.B. Arbeiten im Haushalt, Besorgungen machen, Pflege)."
label variable e2k15Ascale "Ich unterstütze Vater."
label variable e2k15A1 "Ich unterstütze Mutter."
label define e2k15A 1 "Täglich" 2 "Mehrmals in der Woche" 3 "Mehrmals im Monat" 4 "Ein paar Mal im Jahr" 5 "Nie"

exit