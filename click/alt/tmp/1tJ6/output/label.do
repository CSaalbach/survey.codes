﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable krankenhaus "Gab es bei Ihrem Kind in den letzten 12 Monaten gesundheitliche Probleme, die einen Krankenhausaufenthalt notwendig machten?"
 2 ""label define krankenhaus 1 ""
label values krankenhaus krankenhaus

label variable erkrankungen "Ist im Rahmen einer aerztlichen Untersuchung bei Ihrem Kind einmal eine der folgenden Erkrankungen oder Stoerungen festgestellt worden?"
label variable erkrankungen1 "Asthma"
label variable erkrankungen2 "Chronische Bronchitis"
label variable erkrankungen3 "Heuschnupfen"
label variable erkrankungen4 "Schwerhoerigkeit"
label define erkrankungen 1 ""

exit