﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable e1k1 "Ich nehme an der Verlosung des iPad teil."
 2 "Ja"label define e1k1 1 "Nein"
label values e1k1 e1k1

label variable e2k1V "Früher hatten Menschen häufig geringere Bildungschancen als heute. Bitte nennen Sie uns den höchsten Schulabschluss Ihres Vaters und ihrer Ihrer Mutter."
 5 "Volksschul-, Hauptschulabschluss" 4 "Mittlere Reife, 10. Klasse, Realschulabschluss, Fachschulreife" 3 "Fachhochschulreife, fachgebundene Hochschulreife, Abschluss einer Fachoberschule" 2 "Abitur, allgemeine Hochschulreife"label define e2k1V 1 "Anderen Schulabschluss" 0 "Von der Schule abgegangen ohne Abschluss."
label values e2k1V e2k1V

label variable e2k1VS "Anderen Schulabschluss, bitte notieren:"

label variable e2k1M "."
 5 "." 4 "." 3 "." 2 "."label define e2k1M 1 "." 0 "."
label values e2k1M e2k1M

label variable e2k1MS "Anderen Schulabschluss, bitte notieren:"

label variable e2k2V "Bitten nennen Sie uns den höchsten beruflichen Ausbildungsabschluss Ihres Vaters und Ihrer Mutter."
 8 "Keine berufliche Ausbildung (auch Berufsschule ohne Lehre)" 7 "Berufsschule mit gewerblicher oder landwirtschaftlicher Lehre" 6 "Berufsschule mit kaufmännischer oder sonstiger Lehre" 5 "Berufsfachschule" 4 "Meister-, Techniker- oder gleichwertige Fachschule" 3 "Fachhochschule (auch Ingenieurschule)" 2 "Hochschulabschluss"label define e2k2V 1 "Andere berufliche Ausbildung"
label values e2k2V e2k2V

label variable e2k2VS "Andere berufliche Ausbildung, bitte notieren:"

label variable e2k2M "."
 8 "." 7 "." 6 "." 5 "." 4 "." 3 "." 2 "."label define e2k2M 1 "."
label values e2k2M e2k2M

label variable e2k2MS "Andere berufliche Ausbildung, bitte notieren:"

label variable e2k3VS "Bitte erinnern Sie sich: Welche Tätigkeit übten Ihr Vater und Ihre Mutter in ihrem Hauptberuf aus, als Sie selbst 16 Jahre alt waren?"

label variable e2k3MS "."

label variable e2k4VN "Wann wurden Ihre Eltern geboren?"

label define e2k4VN 1 "Kann ich nicht beantworten"
label values e2k4VN e2k4VN

label variable e2k4MN "."

label define e2k4MN 1 "Kann ich nicht beantworten"
label values e2k4MN e2k4MN

label variable e2k5VS "In welchem Land wurden Ihre Eltern geboren?"

label variable e2k5MS "."

label variable e2k6V "Sind Ihre Eltern noch am Leben?"
 2 "Ja"label define e2k6V 1 "Nein"
label values e2k6V e2k6V

label variable e2k6VN "Wann verstorben?"

label variable e2k6M "."
 2 "."label define e2k6M 1 "."
label values e2k6M e2k6M

label variable e2k6MN "Wann verstorben?"

label variable e2k7V "Mit wem lebt Ihr Vater bzw. Ihre Mutter zusammen?"
label variable e2k7V1 "Gemeinsam mit Ihrer Mutter bzw. Vater in einem Haushalt."
label variable e2k7V2 "Mit einer anderen Partnerin bzw. einem anderen Partner in einem Haushalt."
label variable e2k7V3 "Mit jemand anderem in einem Haushalt."
label variable e2k7V4 "Allein."
label variable e2k7V5 "In einem Alters- oder Pflegeheim, in betreuter Wohnanlage."
label variable e2k7V6 "Sonstiges."
label variable e2k7V7 "Kann ich nicht beantworten."
label define e2k7V 1 "."
label values e2k7V e2k7V

label variable e2k7M "."
label variable e2k7M1 "."
label variable e2k7M2 "."
label variable e2k7M3 "."
label variable e2k7M4 "."
label variable e2k7M5 "."
label variable e2k7M6 "."
label variable e2k7M7 "."
label define e2k7M 1 "."
label values e2k7M e2k7M

label variable e2k8V "Wie geht es Ihrem Vater und Ihrer Mutter zurzeit gesundheitlich, insgesamt gesehen?"
 6 "Sehr gut" 5 "Gut" 4 "Es geht so (mittelmäßig)." 3 "Schlecht" 2 "Sehr schlecht"label define e2k8V 1 "Kann ich nicht beantworten"
label values e2k8V e2k8V

label variable e2k8M "."
 6 "." 5 "." 4 "." 3 "." 2 "."label define e2k8M 1 "."
label values e2k8M e2k8M

label variable e2k9V "Benötigen Ihr Vater oder Ihre Mutter innerhalb der letzten 12 Monate regelmäßige Hilfe bei täglichen Verrichtungen, wie z.B. essen, aufstehen, anziehen, baden oder zur Toilette gehen?"
 3 "Ja" 2 "Nein"label define e2k9V 1 "Kann ich nicht beantworten"
label values e2k9V e2k9V

label variable e2k9M "."
 3 "." 2 "."label define e2k9M 1 "."
label values e2k9M e2k9M

label variable e2k10V "Wie oft haben Sie normalerweise Kontakt mit Ihrem Vater und Ihrer Mutter, egal ob persönlich, telefonisch oder schriftlich?"


exit