﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable v225 "Sind Sie ein Mann oder eine Frau?"
label define v225 1 "Eine Frau" 2 "Ein Mann" 9 "Keine Antwort"
label values v225 v225

label variable v226 "Bitten geben Sie Ihr Geburtsjahr an"

label variable v227 "Wurden Sie in Deutschland geboren?"
label define v227 1 "Ja" 2 "Nein" 8 "Weiss nicht" 9 "Keine Antwort"
label values v227 v227

label variable v169 "Haben Sie die deutsche Staatsburgerschaft?"
label define v169 1 "Ja" 2 "Nein" 8 "Weiss nicht" 9 "Keine Antwort"
label values v169 v169

label variable v170 "Wie stolz sind Sie darauf, dass Sie die deutsche Staatsburgerschaft haben? Sind Sie..."
label define v170 1 "Sehr stolz" 2 "Ziemlich stolz" 3 "Nicht sehr stolz" 4 "Uberhaupt nicht stolz" 8 "Weiss nicht" 9 "Keine Antwort"
label values v170 v170

label variable v174 "Welcher politischen Partei stehen Sie am nachsten? "
label define v174 1 "CDU CSU" 2 "SPD" 3 "FDP" 4 "Bundnis 90 Die Grunen" 5 "Die Linke" 6 "AFD" 8 "Weiss nicht" 9 "Keine Antwort"
label values v174 v174

label variable v174a "Einer anderen Partei bitte hier eintragen"

label variable . "."
label variable Q1 "Bitte sagen Sie mir fuer jeden Bereich, ob er Ihnen sehr wichtig, ziemlich wichtig, nicht wichtig oder uberhaupt nicht wichtig ist."
label variable v1 "Arbeit"
label variable v2 "Familie"
label variable v3 "Freunde und Bekannte"
label variable v4 "Freizeit"
label variable v5 "Politik"
label variable v6 "Religion"
label define Q1 1 "Sehr wichtig" 2 "Ziemlich wichtig" 3 "Nicht wichtig" 4 "Ueberhaupt nicht wichtig" 5 "Weiss nicht" 9 "Keine Antwort"
label values Q1* Q1

label variable v7 "Ganz allgemein wuerden Sie sagen Sie sind zurzeit"
label define v7 1 "Sehr gluecklich" 2 "Ziemlich gluecklich" 3 "Nicht sehr gluecklich" 4 "Ueberhaupt nicht gluecklich" 8 "Weiss nicht" 9 "Keine Antwort"
label values v7 v7

label variable v8 "Wie schaetzen Sie insgesamt Ihren Gesundheitszustand ein?"
label define v8 1 "Sehr gut" 2 "Gut" 3 "Durchscnittlich" 4 "Schlecht" 5 "Sehr schlecht" 8 "Weiss nicht" 9 "Keine Antwort"
label values v8 v8

label variable v21 "Wie schaetzen Sie insgesamt Ihren Gesundheitszustand ein?"
label define v21 1 "Ja" 2 "Nein" 8 "Weiss nicht" 9 "Keine Antwort"
label values v21 v21

label variable v21 "Wuerden Sie ganz allgemein sagen, dass man den meisten Menschen vertrauen kann, oder dass man im Umgang mit Menschen nicht vorsichtig genug sein kann? "
label define v21 1 "Man kann den meisten vertrauen " 2 "Man kann nicht vorsichtig genug sein " 8 "Weiss nicht" 9 "Keine Antwort"
label values v21 v21

label variable Q8 "Ich moechte Ihnen jetzt verschiedene Bereiche vorlesen und Sie fragen, wie wichtig diese in Ihrem Leben sind. Bitte sagen Sie mir fuer jeden Bereich, ob er Ihnen sehr wichtig, ziemlich wichtig, nicht wichtig oder ueberhaupt nicht wichtig ist."
label variable v32 "Ihre Familie"
label variable v33 "Menschen in Ihrer Nachbarschaft "
label variable v34 "Menschen, die Sie persoenlich kennen "
label variable v35 "Menschen, denen Sie zum ersten Mal begegnen "
label variable v36 "Menschen anderer Religion "
label variable v37 "Menschen anderer Nationalitaet "
label define Q8 1 "Vertraue voellig" 2 "Vertraue ziemlich" 3 "Vertraue kaum" 4 "Vertraue gar nicht" 5 "Weiss nicht"

exit