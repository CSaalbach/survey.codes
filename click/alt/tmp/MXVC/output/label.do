﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable e9k1 "Wie viele Kinder haben Sie?"

label variable e9k1a "."
label define e9k1a 1 "Ich habe keine Kinder"
label values e9k1a e9k1a

label variable e9k1j1 "Geburtstag"

label variable e9k1j2 "Geburtstag"

label variable e9k1j3 "Geburtstag"

label variable e9k1j4 "Geburtstag"

label variable e9k1j5 "Geburtstag"

label variable e9k1j6 "Geburtstag"

label variable e9k1g1 "Geschlecht"
label define e9k1g1 1 "weiblich" 2 "männlich"
label values e9k1g1 e9k1g1

label variable e9k1g2 "Geschlecht"
label define e9k1g2 1 "weiblich" 2 "männlich"
label values e9k1g2 e9k1g2

label variable e9k1g3 "Geschlecht"
label define e9k1g3 1 "weiblich" 2 "männlich"
label values e9k1g3 e9k1g3

label variable e9k1g4 "Geschlecht"
label define e9k1g4 1 "weiblich" 2 "männlich"
label values e9k1g4 e9k1g4

label variable e9k1g5 "Geschlecht"
label define e9k1g5 1 "weiblich" 2 "männlich"
label values e9k1g5 e9k1g5

label variable e9k1g6 "Geschlecht"
label define e9k1g6 1 "weiblich" 2 "männlich"
label values e9k1g6 e9k1g6

label variable e9k1l1 "leibliches Kind"
label define e9k1l1 1 "Ja" 2 "Nein"
label values e9k1l1 e9k1l1

label variable e9k1l2 "leibliches Kind"
label define e9k1l2 1 "Ja" 2 "Nein"
label values e9k1l2 e9k1l2

label variable e9k1l3 "leibliches Kind"
label define e9k1l3 1 "Ja" 2 "Nein"
label values e9k1l3 e9k1l3

label variable e9k1l4 "leibliches Kind"
label define e9k1l4 1 "Ja" 2 "Nein"
label values e9k1l4 e9k1l4

label variable e9k1l5 "leibliches Kind"
label define e9k1l5 1 "Ja" 2 "Nein"
label values e9k1l5 e9k1l5

label variable e9k1l6 "leibliches Kind"
label define e9k1l6 1 "Ja" 2 "Nein"
label values e9k1l6 e9k1l6

label variable e9k1h1 "lebt zurzeit in meinem Haushalt"
 2 "Ja"label define e9k1h1 1 "Nein"
label values e9k1h1 e9k1h1

label variable e9k1h2 "lebt zurzeit in meinem Haushalt"
 2 "Ja"label define e9k1h2 1 "Nein"
label values e9k1h2 e9k1h2

label variable e9k1h3 "lebt zurzeit in meinem Haushalt"
 2 "Ja"label define e9k1h3 1 "Nein"
label values e9k1h3 e9k1h3

label variable e9k1h4 "lebt zurzeit in meinem Haushalt"
 2 "Ja"label define e9k1h4 1 "Nein"
label values e9k1h4 e9k1h4

label variable e9k1h5 "lebt zurzeit in meinem Haushalt"
 2 "Ja"label define e9k1h5 1 "Nein"
label values e9k1h5 e9k1h5

label variable e9k1h6 "lebt zurzeit in meinem Haushalt"
 2 "Ja"label define e9k1h6 1 "Nein"
label values e9k1h6 e9k1h6

label variable e9k2 "Wie sehr stimmen Sie den folgenden Aussagen zu oder lehnen sie ab?"
label variable e9k2scale "Bei den meisten Kindern muss man öfter einmal hart durchgreifen."
label variable E9k21 "Ohne häufigen Zwang können auch die besten Eltern nichts erreichen."
label variable E9k22 "Kinder müssen immer wieder lernen, auch dann zu gehorchen, wenn sie nicht verstehen, wozu etwas gut ist."
label define e9k2 1 "Lehne völlig ab" 2 "Lehne eher ab" 3 "Stimme eher zu" 4 "Stimme völlig zu"
label values e9k2* e9k2

label variable e10k3 "Wir haben …"
label variable e10k3a "...uns über politische Themen (z.B. Löhne und Preise, Umweltschutz, Kriege, innere Sicherheit) unterhalten."
label variable e10k3b "...gemeinsam Literatur gelesen oder gemeinsam musiziert."
label variable e10k3c "...uns über soziale Themen unterhalten (z.B. Arbeitslosigkeit, Probleme alter und kranker Menschen, Drogenkonsum)."
label variable e10k3d "...Konzerte, Theatervorstellungen, Kunstausstellungen besucht oder über kulturelle Ereignisse gesprochen."
label variable e10k3e "...gemeinsame Ausstellungen oder Vorträge besucht (z.B. über Technik, Wissenschaft, Natur, Verkehr) oder uns über solche allgemeinen Themen unterhalten."
label define e10k3 1 "Fast nie" 2 "Mehrmals im Jahr" 3 "Mehrmals im Monat" 4 "Mehrmals in der Woche"
label values e10k3* e10k3

label variable e10k4 "Welchen Schulabschluss sollte ihr Kind Ihren Vorstellungen nach erreichen? Wenn Ihr Kind bereits einen Schulabschluss hat, kreuzen Sie bitte den Schulabschluss an, den Ihr Kind Ihren Vorstellungen nach hätte erreichen sollen."
label define e10k4 1 "den Hauptschulabschluss" 2 "den Realschulabschluss (Mittlere Reife)" 3 "das Abitur"
label values e10k4 e10k4

label variable e10k5 "Und welchen Berufsabschluss sollte Ihr Kind Ihren Vorstellungen nach erreichen?"
label variable e10k5a "Keinen Abschluss"
label variable e10k5b "Abschluss einer Lehre"
label variable e10k5c "Berufsfachschule, Fachschule (z.B. Gesundheitswesen, Meister-, Technikerschule)"
label variable e10k5d "Fachhochschule"
label variable e10k5e "Universität"
label variable e10k5f "Andere:"
label variable e10k5g "Weiß ich nicht."
label define e10k5 1 "."
label values e10k5 e10k5

label variable e10k5fi "Andere:"

label variable e10k6 "Und was erwarten Sie, welchen Schulabschluss Ihr Kind auch wirklich erreicht? Falls Ihr Kind bereits von der Schule abgegangen ist, kreuzen Sie bitte den Schulabschluss an, den Ihr Kind erreicht hat."
label define e10k6 1 "den Hauptschulabschluss" 2 "den Realschulabschluss (Mittlere Reife)" 3 "das Abitur"
label values e10k6 e10k6

label variable e10k7 "Und was erwarten Sie, welchen Berufsabschluss Ihr Kind auch wirklich erreicht?"
label variable e10k7a "Keinen Abschluss"
label variable e10k7b "Abschluss einer Lehre"
label variable e10k7c "Berufsfachschule, Fachschule (z.B. Gesundheitswesen, Meister-, Technikerschule)"
label variable e10k7d "Fachhochschule"
label variable e10k7e "Universität"
label variable e10k7f "Andere:"
label variable e10k7g "Weiß ich nicht."
label variable e10k7fi "Andere:"

label variable e10k8 "Für wie begabt halten Sie ihr Kind?"
label define e10k8 1 "Sehr begabt" 2 "Eher begabt" 3 "Weniger begabt" 4 "Gar nicht begabt"

exit