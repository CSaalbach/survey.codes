﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable shop "Gab es bei Ihrem Kind in den letzten 12 Monaten gesundheitliche Probleme, die einen Krankenhausaufenthalt notwendig machten?"
 2 "Nein"label define shop 1 "Ja"
label values shop shop

label variable safety "Ist im Rahmen einer ärztlichen Untersuchung bei Ihrem Kind einmal eine der folgenden Erkrankungen oder Störungen festgestellt worden?"
label variable safety1 "Asthma"
label variable safety2 "Chronische Bronchitis"
label variable safety3 "Heuschnupfen"
label define safety 1 "."
label values safety safety

label variable hyg "Wie sehen Sie Ihr Kind heute?"
label variable hygscale "Mein Kind ist meist fröhlich und zufrieden"
label variable hyg1 "Mein Kind ist leicht erregbar und weint häufig"
label variable hyg2 "Mein Kind ist schwer zu trösten"
label variable hyg3 "Mein Kind ist neugierig und aktiv"
label variable hyg4 "Die Gesundheit meines Kindes macht mir Sorgen"
label define hyg 1 "Trifft voll zu" 2 "Trifft eher zu" 3 "Trifft eher nicht zu" 4 "Trifft gar nich zu"

exit