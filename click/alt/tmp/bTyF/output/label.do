﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable feedb "Bitte bewerten Sie die folgenden Aussagen:"
label variable feedbscale "Die Seminarinhalte waren relevant für meine praktische Arbeit"
label variable feedb1 "Die Balance zwischen Input, Kleingruppen-Aktivitäten und Raum für Austausch in der Gesamtgruppen war gelungen"
label variable feedb2 "Mit der Lernatmosphäre fühlte ich mich wohl"
label variable feedb3 "Themen und Bedarfe der Teilnehmenden wurden berücksichtig"
label variable feedb4 "Die Veranstaltung hat mir insgesamt gut gefallen"
label variable feedb5 "Die Inhalte des Online-Unterrichts waren durch die gewählte Form für mich gut zu erbabien"
label define feedb 1 "Trifft zu" 2 "Trifft eher zu" 3 "Trifft eher nicht zu" 4 "Trifft nicht zu"

exit