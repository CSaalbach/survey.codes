﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable Geschlecht "Welches Geschlecht haben Sie?"
 3 "männlich" 2 "weiblich"label define Geschlecht 1 "anderes"
label values Geschlecht Geschlecht

label variable Bildung "Was ist Ihr höchster Bildungsabschluss?"
 6 "Haupt- oder Volksschulabschluss" 5 "Mittlere Reife oder Abschluss der polytechnischen Oberschule" 4 "Abitur, Fachhochschulreife (Gymnasium oder erweiterte Oberschule EOS)" 3 "Universitäts-, Hochschul- bzw. Fachhochschulabschluss" 2 "Promotion"label define Bildung 1 "ohne Schulabschluss"
label values Bildung Bildung

label variable Tätigkeit "Sind Sie im politischen oder politiknahen Bereich beruflich tätig?"
 2 "ja"label define Tätigkeit 1 "nein"
label values Tätigkeit Tätigkeit

label variable Verfahren "Es existieren ja verschiedene Verfahren zur Nominierung von ListenkandidatInnen. Welches wäre Ihrer Meinung nach das beste Verfahren, unabhängig von rechtlichen Fragen."
label variable Verfahren1 "die wahlberechtigten Bürgerinnen und Bürger"
label variable Verfahren2 "alle Mitglieder meiner Partei auf einer Mitgliederversammlung"
label variable Verfahren3 "die gewählten Mitglieder meiner Partei auf einer Delegiertenversammlung"
label variable Verfahren4 "den jeweils zuständigen Parteivorstand"
label define Verfahren 1 "."
label values Verfahren Verfahren

label variable Kand "Haben Sie schon einmal für den Bundestag kandidiert?"
label variable Kandscale "WahlkreiskandidatIn"
label variable Kand1 "im Wahlkreis gewählt"
label variable Kand2 "ListenkandidatIn"
label variable Kand3 "über die Liste gewählt"
label define Kand 1 "Im Jahr 2005" 2 "Im Jahr 2009" 3 "im Jahr 2013" 4 "im Jahr 2017"
label values Kand* Kand

label variable Asp "Wie wichtig sind die folgenden Aspekte für Ihre Kandidatur?"
label variable Aspscale "Politik im Bundestag gestalten"
label variable Asp1 "meine Partei unterstützen"
label variable Asp2 "Aufforderung von einflussreichen ParteifreundInnen zur Kandidatur"
label variable Asp3 "Mangel an gleich geeigneten BewerberInnen"
label variable Asp4 "Wahlkampferfahrungen sammeln"
label variable Asp5 "WählerInneninteressen vertreten"
label variable Asp6 "meine Stellung, mein Ansehen in der Partei verbessern"
label variable Asp7 "das politische System verändern"
label variable Asp8 "innerparteilichen Wettbewerb steigern"
label define Asp 1 "sehr wichtig" 2 "wichtig" 3 "weniger wichtig" 4 "gar nicht wichtig"

exit