﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable shop "Est-ce que l’atelier où vous travaillez appartient à un capitaliste ou à une compagnie d’actionnaires?"
 3 "capitaliste" 2 "compagnie d’actionnaires"label define shop 1 "ne sait pas"
label values shop shop

label variable childage "Quel est âge le plus jeune auquel les enfants sont admis?"
 7 "moins de 13 ans" 6 "13 ans" 5 "14 ans" 4 "15 ans" 3 "16 ans" 2 "plus de 16 ans"label define childage 1 "ne sait pas"
label values childage childage

label variable shoplocal "L’atelier est-il situé à la campagne ou à la ville?"
 2 "à la campagne"label define shoplocal 1 "à la ville"
label values shoplocal shoplocal

label variable agrilabor "Si votre atelier est situé à la campagne est-ce que votre travail industriel suffit à vous faire vivre, ou bien le combinez vous avec un travail agricole?"
 2 "le travail industriel est suffisant pour vivre"label define agrilabor 1 "combiner avec le travail agricole "
label values agrilabor agrilabor

label variable safety "La force motrice, les appareils de transmission et les machines sont-elles protégées de manière à prévenir tout accident?"
label variable safety1 "force motrice"
label variable safety2 "appareils de transmission"
label variable safety3 "machines"
label define safety 1 "."
label values safety safety

label variable hyg "Dans quelle mesure êtes-vous satisfait des conditions d'hygiène de l’atelier?"
label variable hygscale "dimension des pièces"
label variable hyg1 "ventilation"
label variable hyg2 "température"
label variable hyg3 "blanchissement des murs à la chaux"
label variable hyg4 "lieux d’aisance"
label variable hyg5 "propreté générale"
label variable hyg6 "bruit des machines"
label variable hyg7 "poussières métalliques"
label variable hyg8 "humidité"
label define hyg 1 "très satisfait" 2 "plutôt satisfait" 3 "plutôt insatisfait" 4 "très insatisfaits" 5 "ne s'applique pas"
label values hyg* hyg

label variable accidents "Rapportez les accidents arrivés durant votre expérience personnelle."

label variable holidays "Enumérez les jours fériés pendant l’année."



exit