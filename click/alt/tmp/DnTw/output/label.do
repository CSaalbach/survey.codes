﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable v225 "Sind Sie ein Mann oder eine Frau?"
label define v225 1 "Eine Frau" 2 "Ein Mann" 9 "Keine Antwort"
label values v225 v225

label variable v226 "Bitten geben Sie Ihr Geburtsjahr an"

label variable v227 "Wurden Sie in Deutschland geboren?"
label define v227 1 "Ja" 2 "Nein" 8 "Weiss nicht" 9 "Keine Antwort"
label values v227 v227

label variable v169 "Haben Sie die deutsche Staatsburgerschaft?"
label define v169 1 "Ja" 2 "Nein" 8 "Weiss nicht" 9 "Keine Antwort"
label values v169 v169

label variable v170 "Wie stolz sind Sie darauf, dass Sie die deutsche Staatsburgerschaft haben? Sind Sie..."
label define v170 1 "Sehr stolz" 2 "Ziemlich stolz" 3 "Nicht sehr stolz" 4 "Uberhaupt nicht stolz" 8 "Weiss nicht" 9 "Keine Antwort"
label values v170 v170

label variable v174 "Welcher politischen Partei stehen Sie am nachsten? "
label define v174 1 "CDU CSU" 2 "SPD" 3 "FDP" 4 "Bundnis 90 Die Grunen" 5 "Die Linke" 6 "AFD" 8 "Weiss nicht" 9 "Keine Antwort"
label values v174 v174

label variable v174a "Einer anderen Partei bitte hier eintragen"

label variable . "."


exit