﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable krank "Gab es bei Ihrem Kind in den letzten 12 Monaten gesundheitliche Probleme, die einen Krankenhausaufenthalt notwendig machten?"
 2 "Nein"label define krank 1 "Ja"
label values krank krank

label variable erkrank "Ist im Rahmen einer ärztlichen Untersuchung bei Ihrem Kind einmal eine der folgenden Erkrankungen oder Störungen festgestellt worden?"
label variable erkrank1 "Asthma"
label variable erkrank2 "Chronische Bronchitis"
label variable erkrank3 "Heuschnupfen"
label define erkrank 1 "."
label values erkrank erkrank

label variable kind "Wie sehen Sie Ihr Kind heute?"
label variable kindscale "Mein Kind ist meist fröhlich und zufrieden"
label variable kind1 "Mein Kind ist leicht erregbar und weint häufig"
label variable kind2 "Mein Kind ist schwer zu trösten"
label variable kind3 "Mein Kind ist neugierig und aktiv"
label variable kind4 "Die Gesundheit meines Kindes macht mir Sorgen"
label define kind 1 "Trifft voll zu" 2 "Trifft eher zu" 3 "Trifft eher nicht zu" 4 "Trifft gar nich zu"

exit