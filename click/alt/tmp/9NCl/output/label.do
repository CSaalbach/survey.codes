﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable krankh "Gab es bei Ihrem Kind in den letzten 12 Monaten gesundheitliche Probleme, die einen Krankenhausaufenthalt notwendig machte?"
 2 "Nein"label define krankh 1 "Ja"
label values krankh krankh

label variable kranktoffen "und zwar"

label variable erkrankt "Ist im Rahmen einer ärztlichen Untersuchung bei Ihrem Kind einmal eine der folgenden Erkrankungen oder Störungen festgestellt worden?"
label variable erkrankt1 "Asthma"
label variable erkrankt2 "Chronische Bronchitis"
label variable erkrankt3 "Mittelohrentzündung"
label variable erkrankt4 "Heuschnupfen"
label variable erkrankt5 "Neurodermitis"
label define erkrankt 1 "."
label values erkrankt erkrankt

label variable kind "Wie sehen Sie Ihr Kind heute?"
label variable kindscale "Mein Kind ist meist fröhlich und zufrieden"
label variable kind1 "Mein Kind ist leicht erregbar und weint häufig"
label variable kind2 "Mein Kind ist schwer zu trösten"
label variable kind3 "Mein Kind ist neugierig und aktiv"
label variable kind4 "Die Gesundheit meines Kindes macht mir Sorgen"
label define kind 1 "Trifft voll zu" 2 "Trifft eher zu" 3 "Trifft eher nicht zu" 4 "Trifft gar nicht zu"

exit