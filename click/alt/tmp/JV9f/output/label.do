﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable krankenhaus "Gab es bei Ihrem Kind in den letzten 12 Monaten gesundheitliche Probleme, die einen Krankenhausaufenthalt notwendig machten?"
 2 "Nein"label define krankenhaus 1 "Ja"
label values krankenhaus krankenhaus

label variable childage "Was ist das Mindestalter, zu dem Kinder eingestellt werden?"
 7 "jünger als 13 Jahre" 6 "13 Jahre" 5 "14 Jahre" 4 "15 Jahre" 3 "16 Jahre" 2 "älter als 16 Jahre"label define childage 1 "weiß nicht"
label values childage childage

label variable shoplocal "Liegt die Arbeitsstätte auf dem Lande oder in der Stadt?"
 2 "auf dem Lande"label define shoplocal 1 "in der Stadt"
label values shoplocal shoplocal

label variable agrilabor "Falls Ihr Gewerbe auf dem Lande betrieben wird: bildet es Ihre hauptsächliche Erwerbsquelle oder betreiben Sie es zusätzlich zu oder gemeinsam mit der Landwirtschaft?"
 2 "hauptsächliche Erwerbsquelle"label define agrilabor 1 "zusätzlich zu der Landwirtschaft"
label values agrilabor agrilabor

label variable safety "Sind die Antriebskraft, die Transmissionsvorrichtungen und die laufenden Maschinen mit ausreichenden Schutzvorrichtungen gegen Unfälle versehen?"
label variable safety1 "Antriebskraft"
label variable safety2 "Transmissionsvorrichtungen"
label variable safety3 "Maschinen"
label define safety 1 "."
label values safety safety

label variable hyg "Wie sehen Sie Ihr Kind heute?"
label variable hyg1 "Mein Kind ist leicht erregbar"
label variable hyg2 "Mein Kind ist neugierig und aktiv"
label variable hyg3 "Die Gesundheit meines Kindes macht mir Sorgen"
label define hyg 1 "Trifft voll zu" 2 "Trifft eher zu" 3 "Trifft eher nicht zu" 4 "Trifft gar nicht zu"
label values hyg* hyg

label variable accidents "Berichten Sie aus eigener Erfahrung von Unfällen, die Verletzungen bzw. den Tod von Arbeitern verursachten."

label variable holidays "Wieviel Feiertage haben Sie während des Jahres?"



exit