﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable e9k1 "Wie viele Kinder haben Sie?"

label variable e9k2 "Wie sehr stimmen Sie den folgenden Aussagen zu oder lehnen sie ab?"
label variable e9k2scale "Bei den meisten Kindern muss man öfter einmal hart durchgreifen."
label variable e9k21 "Ohne häufigen Zwang können auch die besten Eltern nichts erreichen."
label variable e9k22 "Kinder müssen immer wieder lernen, auch dann zu gehorchen, wenn sie nicht verstehen, wozu etwas gut ist."
label define e9k2 1 "Lehne völlig ab" 2 "Lehne eher ab" 3 "Stimme eher zu" 4 "Stimme völlig zu"
label values e9k2* e9k2

label variable e10k4 "Welchen Schulabschluss sollte ihr Kind Ihren Vorstellungen nach erreichen? Wenn Ihr Kind bereits einen Schulabschluss hat, kreuzen Sie bitte den Schulabschluss an, den Ihr Kind Ihren Vorstellungen nach hätte erreichen sollen."
label define e10k4 1 "den Hauptschulabschluss" 2 "den Realschulabschluss (Mittlere Reife)" 3 "das Abitur"
label values e10k4 e10k4

label variable e10k5 "Und welchen Berufsabschluss sollte Ihr Kind Ihren Vorstellungen nach erreichen?"
label variable e10k51 "Keinen Abschluss"
label variable e10k52 "Abschluss einer Lehre"
label variable e10k53 "Berufsfachschule, Fachschule (z.B. Gesundheitswesen, Meister-, Technikerschule)"
label variable e10k54 "Fachhochschule"
label variable e10k55 "Universität"
label variable e10k56 "Andere:"
label variable e10k57 "Weiß ich nicht."
label define e10k5 1 "."
label values e10k5 e10k5

label variable e10k7 "Und was erwarten Sie, welchen Berufsabschluss Ihr Kind auch wirklich erreicht?"
label variable e10k71 "Keinen Abschluss"
label variable e10k72 "Abschluss einer Lehre"
label variable e10k73 "Berufsfachschule, Fachschule (z.B. Gesundheitswesen, Meister-, Technikerschule)"
label variable e10k74 "Fachhochschule"
label variable e10k75 "Universität"
label variable e10k76 "Andere:"
label variable e10k77 "Weiß ich nicht."
label variable e10k8 "Für wie begabt halten Sie ihr Kind?"
label define e10k8 1 "Sehr begabt" 2 "Eher begabt" 3 "Weniger begabt" 4 "Gar nicht begabt"

exit