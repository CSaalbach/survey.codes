﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable Familienstand "Welchen Familienstand haben Sie derzeit?"
 5 "Verheiratet und lebe mit EhepartnerIn zusammen" 4 "Verheiratet, aber in Trennung lebend" 4 "Geschieden " 2 "Verwitwet "label define Familienstand 1 "Ledig"
label values Familienstand Familienstand

label variable Hhalt "Wie setzt sich Ihr Haushalt gegenwärtig zusammen? Kreuzen Sie bitte an, wer alles bei Ihnen im gleichen Haushalt lebt."
label variable Hhalt01 "Ich wohne alleine."
label variable Hhalt02 "EhepartnerIn bzw. LebenspartnerIn."
label variable Hhalt03 "Eigene Kinder (leibliche Kinder und Adoptivkinder)"
label variable Hhalt04 "Stiefkinder (Kinder des (Ehe-)partners)"
label variable Hhalt05 "Eigene Eltern, Schwiegereltern"
label define Hhalt 1 "."

exit