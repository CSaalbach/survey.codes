﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable v225 "Sind Sie ein Mann oder eine Frau?"
label define v225 1 "Eine Frau" 2 "Ein Mann" 9 "Keine Antwort"
label values v225 v225

label variable v226 "Bitten geben Sie Ihr Geburtsjahr an"

label variable v227 "Wurden Sie in Deutschland geboren?"
label define v227 1 "Ja" 2 "Nein" 8 "Weiss nicht" 9 "Keine Antwort"
label values v227 v227

label variable v169 "Haben Sie die deutsche Staatsburgerschaft?"
label define v169 1 "Ja" 2 "Nein" 8 "Weiss nicht" 9 "Keine Antwort"
label values v169 v169

label variable v170 "Wie stolz sind Sie darauf, dass Sie die deutsche Staatsburgerschaft haben? Sind Sie..."
label define v170 1 "Sehr stolz" 2 "Ziemlich stolz" 3 "Nicht sehr stolz" 4 "Uberhaupt nicht stolz" 8 "Weiss nicht" 9 "Keine Antwort"
label values v170 v170

label variable v174 "Welcher politischen Partei stehen Sie am nachsten? "
label define v174 1 "CDU CSU" 2 "SPD" 3 "FDP" 4 "Bundnis 90 Die Grunen" 5 "Die Linke" 6 "AFD" 8 "Weiss nicht" 9 "Keine Antwort"
label values v174 v174

label variable v174a "Einer anderen Partei bitte hier eintragen"

label variable Q1 "Bitte sagen Sie mir fuer jeden Bereich, ob er Ihnen sehr wichtig, ziemlich wichtig, nicht wichtig oder uberhaupt nicht wichtig ist."
label variable v1 "Arbeit"
label variable v2 "Familie"
label variable v3 "Freunde und Bekannte"
label variable v4 "Freizeit"
label variable v5 "Politik"
label variable v6 "Religion"
label define Q1 1 "Sehr wichtig" 2 "Ziemlich wichtig" 3 "Nicht wichtig" 4 "Ueberhaupt nicht wichtig" 8 "Weiss nicht" 9 "Keine Antwort"
label values Q1* Q1

label variable v7 "Ganz allgemein wuerden Sie sagen Sie sind zurzeit"
label define v7 1 "Sehr gluecklich" 2 "Ziemlich gluecklich" 3 "Nicht sehr gluecklich" 4 "Ueberhaupt nicht gluecklich" 8 "Weiss nicht" 9 "Keine Antwort"
label values v7 v7

label variable v8 "Wie schaetzen Sie insgesamt Ihren Gesundheitszustand ein?"
label define v8 1 "Sehr gut" 2 "Gut" 3 "Durchschnittlich" 4 "Schlecht" 5 "Sehr schlecht" 8 "Weiss nicht" 9 "Keine Antwort"
label values v8 v8

label variable v21 "Waren Sie in den vergangenen 6 Monaten ehrenamtlich taetig"
label define v21 1 "Ja" 2 "Nein" 8 "Weiss nicht" 9 "Keine Antwort"
label values v21 v21

label variable v31 "Wuerden Sie ganz allgemein sagen, dass man den meisten Menschen vertrauen kann, oder dass man im Umgang mit Menschen nicht vorsichtig genug sein kann? "
label define v31 1 "Man kann den meisten vertrauen " 2 "Man kann nicht vorsichtig genug sein " 8 "Weiss nicht" 9 "Keine Antwort"
label values v31 v31

label variable Q8 "Ich moechte Ihnen jetzt verschiedene Bereiche vorlesen und Sie fragen, wie wichtig diese in Ihrem Leben sind. Bitte sagen Sie mir fuer jeden Bereich, ob er Ihnen sehr wichtig, ziemlich wichtig, nicht wichtig oder ueberhaupt nicht wichtig ist."
label variable v32 "Ihre Familie"
label variable v33 "Menschen in Ihrer Nachbarschaft "
label variable v34 "Menschen, die Sie persoenlich kennen "
label variable v35 "Menschen, denen Sie zum ersten Mal begegnen "
label variable v36 "Menschen anderer Religion "
label variable v37 "Menschen anderer Nationalitaet "
label define Q8 1 "Vertraue voellig" 2 "Vertraue ziemlich" 3 "Vertraue kaum" 4 "Vertraue gar nicht" 8 "Weiss nicht" 9 "Keine Antwort"
label values Q8* Q8

label variable v38 "Bitte geben Sie an, wieviel Entscheidungsfreiheit und Einfluss Sie nach Ihrem Empfinden darauf haben, wie Ihr weiteres Leben ablaeuft? "
label define v38 1 "Ueberhaupt keine Freiheit" 2 "" 3 "" 4 "" 5 "" 6 "" 7 "" 8 "" 9 "" 10 "Voellige Freiheit" 88 "Weiss nicht" 99 "Keine Antwort"
label values v38 v38

label variable v39 "Wenn Sie einmal alles beruecksichtigen, wie zufrieden sind Sie insgesamt zurzeit mit Ihrem Leben?"
label define v39 1 "Ueberhaupt nicht zufrieden" 2 "" 3 "" 4 "" 5 "" 6 "" 7 "" 8 "" 9 "" 10 "Voellig zufrieden" 88 "Weiss nicht" 99 "Keine Antwort"
label values v39 v39

label variable Q11 "Hier steht Verschiedenes ueber die berufliche Arbeit. Bitte sagen Sie mir, was davon Sie persoenlich an einem Beruf fuer ganz besonders wichtig halten"
label variable v40 "Gute Bezahlung"
label variable v41 "Angenehme Arbeitszeiten"
label variable v42 "Die Moeglichkeit eigene Initiative zu entfalten"
label variable v43 "Grosszuegige Urlaubsreglungen"
label variable v44 "Ein Beruf bei dem man das Gefuehl hat etwas erreichen zu koennen"
label variable v45 "Ein Beruf mit Verantwortung"
label define Q11 1 "."
label values Q11 Q11

label variable v51 "Gehoeren Sie einer Religionsgemeinschaft an?"
label define v51 1 "Ja Weiter mit Q14" 2 "Nein" 8 "Weiss nicht  Weiter mit Q14" 9 "Keine Antwort Weiter mit Q14"
label values v51 v51

label variable v52 "Welcher Religionsgemeinschaft gehoeren Sie an?"
label define v52 1 "Der roemisch katholischen Kirche" 2 "Der evangelischen Kirche" 3 "Einer evangelischen Freikirche" 4 "Der griechisch orthodoxen Kirche" 5 "Der russisch orthodoxen Kirche" 6 "Dem Islam" 88 "Weiss nicht" 99 "Keine Antwort"
label values v52 v52

label variable v52a "Einer anderen Religionsgemeinschaft, und zwar bitte hier eintragen"

label variable v54 "Haben Sie jemals einer Religionsgemeinschaft angehoert?"
label define v54 1 "Ja" 2 "Nein" 3 "Weiss nicht" 4 "Keine Antwort"
label values v54 v54

label variable Q28 "Hier steht Verschiedenes ueber die berufliche Arbeit. Bitte sagen Sie mir, was davon Sie persoenlich an einem Beruf fuer ganz besonders wichtig halten"
label variable v85 "Gutes Benehmen"
label variable v86 "Unabhaengigkeit Selbststaendigkeit"
label variable v87 "Fleiss"
label variable v88 "Verantwortungsgefuehl"
label variable v89 "Vorstellungskraft Fantasie"
label variable v90 "Toleranz und Respekt gegenueber Mitmenschen"
label variable v91 "Sparsamkeit im Umgang mit Geld und Dingen"
label variable v92 "Entschlossenheit, Ausdauer"
label variable v93 "Religioeser Glaube"
label variable v94 "Selbstlosigkeit"
label variable v95 "Gehorsam"
label define Q28 1 "."

exit