﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable sex "Welches Geschlecht haben Sie?"
 4 "weiblich" 3 "männlich" 2 "divers"label define sex 1 "keine Angabe"
label values sex sex

label variable age "Wie alt sind Sie?"

label variable sport "Wie oft in der Woche machen Sie Sport?"
 5 "0 Mal" 4 "1 bis 2 Mal" 3 "3 bis 4 Mal" 2 "5 bis 6 Mal"label define sport 1 "täglich"
label values sport sport

label variable alc1 "Trinken Sie Alkohol?"
 2 "Ja"label define alc1 1 "Nein"
label values alc1 alc1

label variable alc2 "Wenn ja, wie oft in der Woche?"
 5 "unregelmäßig" 4 "1 bis 2 Mal" 3 "3 bis 4 Mal" 2 "5 bis 6 Mal"label define alc2 1 "täglich"
label values alc2 alc2

label variable smoking1 "Rauchen Sie?"
 2 "Ja"label define smoking1 1 "Nein"
label values smoking1 smoking1

label variable smoking2 "Wenn ja, wie oft in der Woche?"
 5 "unregelmäßig" 4 "1 bis 2 Mal" 3 "3 bis 4 Mal" 2 "5 bis 6 Mal"label define smoking2 1 "täglich"
label values smoking2 smoking2

label variable sleep "Wieviele Stunden schlafen Sie am Tag?"

label variable habits "Für wie nützlich halten Sie die folgenden Dinge zur Förderung der Gesundheit?"
label variable habitsscale "Joggen"
label variable habits1 "Meditieren"
label variable habits2 "Schlafen"
label variable habits3 "Schwimmen"
label variable habits4 "Gesunde Ernährung"
 5 "sehr nützlich" 4 "eher nützlich" 3 "teils, teils" 2 "eher unnützlich"label define habits 1 "sehr unnützlich"

exit