﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable Familienstand "Welchen Familienstand haben Sie derzeit?"
 5 "Verheiratet und lebe mit EhepartnerIn zusammen" 4 "Verheiratet, aber in Trennung lebend" 3 "Geschieden " 2 "Verwitwet "label define Familienstand 1 "Ledig"
label values Familienstand Familienstand

label variable Hhalt "Wie setzt sich Ihr Haushalt gegenwärtig zusammen? Kreuzen Sie bitte an, wer alles bei Ihnen im gleichen Haushalt lebt."
label variable Hhalt1 "Ich wohne alleine."
label variable Hhalt2 "EhepartnerIn bzw. LebenspartnerIn."
label variable Hhalt3 "Eigene Kinder (leibliche Kinder und Adoptivkinder)"
label variable Hhalt4 "Stiefkinder (Kinder des (Ehe-)partners)"
label variable Hhalt5 "Eigene Eltern, Schwiegereltern"
label define Hhalt 1 "."
label values Hhalt Hhalt

label variable HHalt6 "Andere Personen: "

label variable Beziehung "Wie gestalten sich die Beziehungen zu Ihrer jetzigen (Ehe-)Partnerin bzw. Ihrem jetzigen (Ehe-)Partner? In unserer Ehe bzw. In unserer Partnerschaft …"
label variable Beziehungscale "… kann ich ihr bzw. ihm alles erzählen, was mich beschäftigt."
label variable Beziehung1 "… gibt es Missstimmungen und Spannungen."
label variable Beziehung2 "… findet sie bzw. er gut, was ich mache. "
label variable Beziehung3 "… kommt wegen Kleinigkeiten gereizte Stimmung auf."
 6 "Immer" 5 "Sehr oft" 4 "Oft" 3 "Manchmal" 2 "Selten "label define Beziehung 1 "Nie "
label values Beziehung* Beziehung

label variable ArbeitHhalt "Wie teilen Sie mit Ihrer (Ehe-)Partnerin bzw. Ihrem (Ehe-)Partner die Arbeiten im gemeinsamen Haushalt?"
label variable ArbeitHhaltscale "Zubereiten der Mahlzeiten"
label variable ArbeitHhalt2 "Spülen und aufräumen nach den Mahlzeiten"
label variable ArbeitHhalt3 "Lebensmittel einkaufen"
label variable ArbeitHhalt4 "Putzen der Wohnung und des Hauses"
label variable ArbeitHhalt5 "Wäsche waschen"
 7 "Stets die Frau" 6 "Meist die Frau" 5 "Etwa öfter die Frau" 4 "Etwas öfter der Mann" 3 "Meist der Mann" 2 "Stets der Mann"label define ArbeitHhalt 1 "Andere Person(en)"
label values ArbeitHhalt* ArbeitHhalt

label variable Schulabschluss "Nennen Sie uns bitte den höchsten Schulabschluss Ihrer (Ehe-)Partnerin bzw. Ihres (Ehe-)Partners. Wenn zur Zeit keine Partnerschaft: Bitte für die letzte Partnerschaft ausfüllen. "
 6 "Volksschul-, Hauptschulabschluss" 5 "Mittlere Reife, 10. Klasse, Realschulabschluss, Fachschule" 4 "Fachhochschulreife, fachgebundene Hochschulreife, Abschluss einer Fachoberschule" 3 "Abitur, allgemeine Hochschulreife" 2 "Von der Schule abgegangen ohne Abschluss."label define Schulabschluss 1 "Anderer Schulabschluss"
label values Schulabschluss Schulabschluss

label variable berufTätigkeit1 "Durchschnittliche Arbeitszeit der Partnerin bzw. des Partners je Woche:"

label variable berufTätigkeit3 "Monatliches Nettoeinkommen in Euro"



exit