﻿// This do-file was automatically created using the CLiCKSurveyAMC software //

cd //Enter your path
version //Enter your version
clear all
set more off
import delimited "content.csv"

/*------------------------------*/


label variable var40 "Kennen Sie das Fundbüro der Stadt Leipzig im Technischen Rathaus, Prager Straße? "
label define var40 1 "ja" 2 "nein" -99 "weiß nicht"
label values var40 var40

label variable var41 "Haben Sie das Fundbu?ro der Stadt Leipzig in den letzten 5 Jahren genutzt?"
label variable var41a "ja, ich habe etwas gefunden "
label variable var41b "ja, ich habe etwas verloren "
label variable var41c "nein"
label define var41 1 "."
label values var41 var41

label variable var43 "Wenn Sie das Fundbüro in den letzten 5 Jahren genutzt haben: Wie zufrieden sind Sie mit den folgenden Aspekten der Nutzung?"
label variable var43a "Öffnungszeiten"
label variable var43b "telefonische Erreichbarkeit"
label variable var43c "Onlineterminvergabe"
label variable var43d "Kommunikation per E-Mail"
label define var43 1 "sehr zufrieden" 2 "eher zufrieden" 3 "teils/teils" 4 "eher unzufrieden" 5 "sehr unzufrieden" -99 "weiß nicht"
label values var43* var43

label variable var18 "Sind Sie?"
 0 "männlich"label define var18 1 "weiblich" -88 "keine Angaben"
label values var18 var18

label variable var20 "Seit wann haben Sie ununterbrochen Ihren Hauptwohnsitz in Leipzig oder in einem der seit 1990 eingemeindeten Ortsteile? Bitte geben Sie das Jahr an"

label variable var26 "Welche der folgenden Angaben trifft fu?r Sie hauptsa?chlich zu? "
label define var26 1 "Erwerbsta?tige/-r (auch Auszubildende/-r, geringfu?gig Bescha?ftigte/-r, o?ffentlich gefo?rderte Bescha?ftigung, Altersteil-, Elternzeit)" 2 "Arbeitslose/-r, Arbeitssuchende/-r bzw. in einer Umschulungsmaßnahme befindlich" 3 "Rentner/-in oder Pensiona?r/-in (Alters- und Erwerbsunfa?higkeitsrente)" 4 "Student/-in oder Schu?ler/-in " 5 "etwas anderes (z. B. Hausfrau/Hausmann)" -88 "keine Angaben"
label values var26 var26

label variable var28 "Welchen höchsten beruflichen Abschluss haben Sie?"
label define var28 1 "Universita?ts-/Hochschul-/Fachhochschulabschluss " 2 "Meister-/Technikerausbildung oder Fachschulabschluss (auch der ehem. DDR) " 3 "abgeschlossene Berufsausbildung, Teilfacharbeiter/-in " 4 "noch Studierende/r noch Auszubildende/-r " 5 "ohne abgeschlossene Berufsausbildung " -88 "keine Angaben"

exit