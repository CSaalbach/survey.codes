#!/bin/bash
pdflatex gen_codebook.tex

rm *.log *.aux

pdflatex gen_quest.tex

rm *.log *.aux

pdftotext gen_quest.pdf questionnaire.tex

pdflatex questionnaire.tex

rm *.log *.aux *.amc

#mkdir output

mv gen_codebook.pdf output/codebook.pdf

mv gen_quest.pdf output/gen_quest.pdf

mv questionnaire.pdf output/questionnaire.pdf

mv questionnaire.tex output/questionnaire.tex

cp gen_codebook.tex output/gen_codebook.tex

cp content.csv output/content.csv

cp surveyamc.sty output/surveyamc.sty

#zip -r output.zip output
