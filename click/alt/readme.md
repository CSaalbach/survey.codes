# CLiCKSurveyAMC
### Claudia Saalbach
###### Contributor: Johannes Schütt
<br/>
<b>index.php</b>: The main file that includes the GUI of the program aswell as the uploading routine.
<br/><br/>
<b>gen_stata.php</b>: The script responsible for generating the STATA do-file.
<br/><br/>
<b>gen_chars.php</b>: Contains an array of special characters and their LaTeX Code for conversion.
<br/><br/>
The main program is located in the "scripts" folder.
