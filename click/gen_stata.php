<?php
## STATA Labler ##
## Johannes Schütt, johschuett@uni-potsdam.de

error_reporting(0); # Debugging


### READ SOURCE

$source = $path . "/content.csv"; # Source csv file
$file = file($source); # For count($file);

$h = fopen($source, 'r'); # Open handle to read

for ($i = 0; $i <= count($file); $i++) {
  $name = "row" . $i; # Create name for variable
  $line = fgets($h, 1000); # Get current line
  $line = explode("\t", $line); # Split line into columns
  $$name = [$line[1], $line[2], $line[3], $line[4]]; # Save content of curent row
}

fclose($h); # Close handle

### END READ SOURCE


### PUT DATA IN DO-FILE

$out = $path . "/output/label.do"; # Path of output file
$h = fopen($out, 'wb'); # Open handle to write
fwrite($h, pack("CCC",0xef,0xbb,0xbf)); # UTF-8 Encoding

$head = "// This do-file was automatically created using the CLiCKSurveyAMC software //\n
cd //Enter your path
version //Enter your version
clear all
set more off
import delimited \"content.csv\"\n
/*------------------------------*/\n
\n"; # Create head

$head  = mb_convert_encoding($head, 'UTF-8'); # Convert to UTF-8
fwrite($h, $head); # Write head


$answer = FALSE;

for ($i = 0; $i <= count($file); $i++) {
  $name = "row" . $i; # Get current row

  $class = $$name[0];
  $type = $$name[1];
  $var = $$name[2];
  $text = str_replace("\n", "", $$name[3]); # Remove linebreak at the end of the string

  if ($class == "Q" OR $class == "SQ") { # Row contains question or subquestion
    if ($class == "Q") { # Row contains question
      if ($answer == TRUE) { # Last row contained an answer
        if ($lastType == "F") { # Last question was a matrix question
          $com = "\nlabel values " . $lastVar . "* " . $lastVar . "\n\n"; # Create command
          $com  = mb_convert_encoding($com, 'UTF-8'); # Convert to UTF-8
        }

        else {
          $com = "\nlabel values " . $lastVar . " " . $lastVar . "\n\n"; # Create command
          $com  = mb_convert_encoding($com, 'UTF-8'); # Convert to UTF-8
        }

        fwrite($h, $com); # Write command

        $answer = FALSE;
      }
      $lastType = $type; # Save type of question for answer(s)
      $lastVar = $var; # Save var of question for answer(s)
    }

    $com = "label variable " . $var . " \"" . $text . "\"\n"; # Create command
    $com  = mb_convert_encoding($com, 'UTF-8'); # Convert to UTF-8
    fwrite($h, $com); # Write command

    if ($type == "N" OR $type == "S") {
      $com = "\n"; # Create newline after numerical or string question
      $com  = mb_convert_encoding($com, 'UTF-8'); # Convert to UTF-8
      fwrite($h, $com); # Write command
    }
  }

  else if ($class == "A") { # Row contains answer
    $answer = TRUE;

    if ($var == 1) { # First answer
      $com = "label define " . $lastVar . " " . $var . " \"" . $text . "\""; # Create command
      $com  = mb_convert_encoding($com, 'UTF-8'); # Convert to UTF-8
      fwrite($h, $com); # Write command
    }

    else { # Not the first answer
      $com = " " . $var . " \"" . $text . "\""; # Add additional answer
      $com  = mb_convert_encoding($com, 'UTF-8'); # Convert to UTF-8
      fwrite($h, $com); # Write command
    }

  }
}

$foot = "\n\nexit"; # Write foot
$foot  = mb_convert_encoding($foot, 'UTF-8'); # Convert to UTF-8
fwrite($h, $foot); # Write foot

fclose($h); # Close handle

### END PUT DATA IN TEMPLATE

?>
