<!-- ClickAMC stable-1.0                                                
Claudia Saalbach, Johannes Schütt, 
Chair of Empirical Methods, University of Potsdam 
License: GNU General Public License v2.0 only     
 -->

<?php
# start php ----------------------------------
error_reporting(0); # DEBUGGING

define('ABSPATH', dirname(__FILE__).'/');
define('access', TRUE); # access to counter.php
include("gen_chars.php"); # include LaTeX chars

# check url -----------------------------------
if ($_SERVER['HTTP_HOST'] != "survey.codes") {
       header("Location: https://survey.codes/click");
}

# function start with -------------------------
function startsWith($haystack, $needle) {
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

# function for randomized name ----------------
function getName($n) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';

    for ($i = 0; $i < $n; $i++) {
        $index = rand(0, strlen($characters) - 1);
        $randomString .= $characters[$index];
    }

    return $randomString;
}

# prepare directory ----------------------------
$dir = getName(4); # create random 4-digit folder name
while (file_exists("tmp/" . $dir)) { # check if folder name is already taken
    $dir = getName(4);
}

# define variables -----------------------------
$target_file = "tmp/" . $dir . "/content.csv";
$err = ""; # error message
$suc = ""; # success message
$title = "ClickAMC"; # page title
$upload = 1;

# start procedure ------------------------------
if(isset($_POST['btn'])) { 

  # checks ====================================
  if (true) { # get file extension
    $fileInfo = pathinfo($_FILES["datei"]["name"]);
    $ext = strtolower($fileInfo["extension"]);
    
    ### 1. check if file is selected ************
    if ($_FILES["datei"]["name"] == "") {
      $upload = 0;
      $err = '
              <div class="row justify-content-center error">
                No file selected.                                
              </div>';
    }

    ### 2. check file extension *****************
    else if ($ext != "csv" && $ext != "txt" && $ext != "tsv") {
      $upload = 0;
      $err = '
              <div class="row justify-content-center error">                
                Your file is not a CSV, TSV or TXT file.
              </div>';
    }

    ### 3. check file size **********************
    else if ($_FILES["datei"]["size"] > 1000000) {
      $upload = 0;
      $err = '
              <div class="row justify-content-center error">                 
                Your file is bigger then 1 MB.                
              </div>';
    }

    ### if all checks passed ++++++++++++++++++++

    if ($upload == 1) {
      $path = "tmp/" . $dir;
      ## variables *******************************
      $check = "id\tclass\ttype/scale\tname\ttext"; # meta structure 
      $del = "rm -r " . $path; # remove if error                

      ### make directories ************************  
      mkdir($path);
      chmod($path, 0777);            

      mkdir($path . "/csv"); # for options.csv
      chmod($path . "/csv", 0777);

      ### check if missing is set *****************
      if ($_POST['missing']==""){
        $missing=99;
      }
      else {
        $missing=$_POST['missing'];
      }

      ### check if layout is set *****************
      if ($_POST['layout']==""){
        $layout=1;
      }
      else {
        $layout=$_POST['layout'];
      }      

      ### write options.csv ***********************
      $h = fopen($path . "/csv/options.csv", 'w');
      fwrite($h, "layout,missing\n" . $_POST['layout'] . "," . $missing . "\n");
      fclose($h);

      ### move upload to tmp **********************
      if (move_uploaded_file($_FILES["datei"]["tmp_name"], $target_file)) {

        #### check meta structure :::::::::::::::::
        $h = fopen($target_file, "r"); # get column names
          $firstline = fgets($h, 1000);
        fclose($h);

        if (startsWith($firstline, $check)) { # check meta

          mkdir($path . "/output"); # make output dir
          include("gen_stata.php"); # include stata labler

          $h = fopen($target_file, "r"); # open handle
            $file = file($target_file); # for count($file);
            for ($i = 0; $i <= count($file); $i++) {
              $line = fgets($h, 1000); # get current line
              $line = str_replace("\\", "\\textbackslash ", $line);
              $clean[] = $line; # put converted line in array
            }            
          fclose($h); # close handle

          $h = fopen($target_file, "w"); # open handle
            for ($i = 0; $i < count($clean); $i++) {
              $t = $clean[$i];
              fwrite($h, $t); # write
            }          
          fclose($h); # close handle

          ##### check character ...................  
          if(isset($_POST['chars'])) { # check input "chars"
            $h = fopen($target_file, "r"); # open handle
              $file = file($target_file); # for count($file);
              for ($i = 0; $i <= count($file); $i++) {
                $line = fgets($h, 1000); # get current line
                $line = str_replace(array_keys($chars), array_values($chars), $line); # replace char with the TEX from charconverter.php
                $data[] = $line; # put converted line in array
              }
            fclose($h); # close handle

            $h = fopen($target_file, "w"); # open handle
              for ($i = 0; $i < count($data); $i++) {
                $t = $data[$i];
                fwrite($h, $t); # write
              }
            fclose($h); # close handle

          } # end check character ................. 

        } # end check meta ::::::::::::::::::::::::

        ## copy scripts to tmp/random *************
        copy("scripts/autosurvey.sh", $path . "/autosurvey.sh");
        copy("scripts/gen_codebook.tex", $path . "/gen_codebook.tex");
        copy("scripts/quest_generator.R", $path . "/quest_generator.R");
        copy("scripts/automultiplechoice.sty", $path . "/automultiplechoice.sty");
        copy("scripts/questionnaire.tex", $path . "/questionnaire.tex");

        ## run scripts ****************************
        chdir($path); # move to tmp/random        
        exec("chmod -R 777 ."); # permissions to all copied files
	      exec("Rscript --encoding=UTF-8 quest_generator.R");       
	      exec("sh autosurvey.sh"); # run shell

        ## error messages *************************
        if (!file_exists(ABSPATH. $path . "/output/codebook.pdf") OR !file_exists(ABSPATH . $path . "/output/questionnaire.pdf") OR !file_exists(ABSPATH . $path . "/output/label.do")) {

            if(isset($_POST['chars'])) {
              $err = '
                <div class="row justify-content-center error">
                  Please check the column names of your meta file and if the file is tab-delimited.
                    <br>
                  For help take a look at the sample meta files below or the <a href="https://survey.codes/pdf/surveyamc_manual.pdf" target="_blank">Manual</a>                  
                </div>';

              chdir("../../"); # switch back to root
              exec($del); # removing tmp
            }
            else {
              $err = '
                <div class="row justify-content-center error mx-auto" style="width:50%">  
                <p>              
                  Try "Convert to TEX characters" or check the structure of your meta file.
                </p>
                <p>  
                  For help, you can take a look at the sample files below or at the
                   <a href="https://survey.codes/pdf/surveyamc_manual.pdf" target="_blank">
                      Manual
                    </a>.                                    
                 </p>   
                </div>';
              chdir("../../"); # switch back to root
              exec($del); # removing tmp
            }
        }

        ## name ZIP upload ************************
        else {
          # name ZIP with upload file name
          $zipname = utf8_decode(basename($_FILES["datei"]["name"], ".csv")); 
          # rename if original name contains only alphanumerical characters
          $zipname = preg_replace("/[^a-zA-Z0-9]+/", "", $zipname); 

        ## if CSV name contais non-alphanumerical characters, rename ZIP to "output.zip"
          if($zipname == "") {
            $zipname = "output";
          }

        ## create ZIP upload **********************
          $zipname = $zipname . ".zip";

          $zip = new ZipArchive;

        ## add files to ZIP ***********************          
          if ($zip->open($zipname, ZipArchive::CREATE) === TRUE) {
            $zip->addFile('output/codebook.pdf');
            $zip->addFile('output/content.csv');
            $zip->addFile('output/gen_codebook.tex');
            $zip->addFile('output/quest_generator.R');
            $zip->addFile('output/questionnaire.tex');
            $zip->addFile('output/questionnaire.pdf');
            $zip->addFile('output/automultiplechoice.sty');
            $zip->addFile('output/label.do');
            $zip->addFile('output/csv/options.csv');

            $zip->close(); # close ZIP ************
          } # end add files to ZIP ****************
        
        ## switch back to root ********************
        chdir("../../");
        
        ## success message and download ***********
        $suc = '
          <div class="row justify-content-center">
            <div class="mx-auto" style="              
              width:600px;
              text-align: center; 
              margin-bottom: 5%;              
              color: #2C3744; 
              border: 1px dashed #28a745;
              border-radius: 5px;
              padding-top: 1.5%;
              padding-bottom: 1.5%;
              font-weight: 550;">
                Woohoo! Your survey products are created. 
                <br>
                You can download them here:
                <br>                
                
                <a href="' . $path . '/' . $zipname . '"><button style="margin-top:2%;" type="button" class="btn btn-success">Download</button>
                </a>     
            </div>
           </div> 
        ';
        } # end name ZIP upload *******************
      } # end move upload to tmp ****************** 



    # error message if column names do not match meta structure   
    else {
      $err = '
              <div class="row justify-content-center error">                
                <p>
                Your file is not a CSV, TSV or TXT file.
                </p>
                <p>
                Please check the column names of your meta file and if the file is tab-delimited. For help take a look at the sample meta files below or the <a href="https://survey.codes/pdf/surveyamc_manual.pdf" target="_blank">Manual</a>
                </p>          
              </div>';
      exec($del); # removing tmp dir
    }

    } # end all checks passed +++++++++++++++++++++    

  } # end checks ==================================
    
  # error message upload failed ===================
  else {
    $err = '
            <div class="row justify-content-center error">
              Oops, something went wrong!            
            </div>';
    exec($del); # Removing tmp dir
  } 

} # end start procedure ---------------------------
?>

<!-- start HTML ---------------------------------->
<!doctype html>
<html>
  <head>
    <meta charset="utf-8"/>
    <meta content="IE=edge" http-equiv="X-UA-Compatible"/>
    <meta content="width=device-width, initial-scale=1" name="viewport"/>

    <title><?php echo $title; ?></title>
      
    <link href="../favicon.ico" rel="shortcut icon" type="image/x-icon"/>
      
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">      
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script type="text/javascript" src="js/bootstrap-filestyle.min.js"> </script>
<!-- css style sheet ------->
    <link href="../styles.css" rel="stylesheet">
<!-- fonts ----------------->        
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,500|Roboto:400,500&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400&display=swap" rel="stylesheet"> 
    <style>
      .row {
        margin-left: -5px;
        margin-right: -5px;        
      }
      .row > form {
        border: 1px solid #F3C71E;
        border-radius: 5px;
        margin-top: 2.5%;
        margin-bottom: 2.5%;
        padding:5%; 
      }
      .formbox {
        border: 1px solid #F3C71E;
        border-radius: 5px;
      }

      .btn-primary {
        color: #fff;
        background-color: #007bff;
        border-color: #007bff;
      }

      .btn-primary:hover {
        color: #fff;
        background-color: #0069d9;
        border-color: #0062cc;
      }

      .btn-primary:focus, .btn-primary.focus {
        color: #fff;
        background-color: #0069d9;
        border-color: #0062cc;
        box-shadow: 0 0 0 0.2rem rgba(38, 143, 255, 0.5);
      }

      .error{
        text-align: center; 
        margin-top: 2.5%;
        margin-bottom:5%;        
        color: #CD4F39; 
        font-size: 80%;
        border: 1px dashed #CD4F39;
        border-radius: 5px;
        padding: 3%;        
      }
      .carousel-inner{
        background-color: #f8f9fa !important;
        width:100%;
        /*border: 1px solid #F3C71E;*/
        border-radius: 5px;        
      } 
    </style>   
</head>

<body>
  <header>
    <nav class="navbar navbar-expand-md navbar-light bg-light">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      </button>
      
        <a class="navbar-brand" href="https://survey.codes/">survey.codes</a>

        <div class="collapse navbar-collapse" id="navbarTogglerDemo03">

        <div class="container">
          
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0 justify-content-center">
                <li class="nav-item">
                  <a class="nav-link" href="../surveyamc">SurveyAMC</a>
                </li>

                <li class="nav-item active">
                  <a class="nav-link" href="../click">ClickAMC</a>
                </li>

                <li class="nav-item">
                  <a class="nav-link" href="../datatblr">Datatblr</a>
                </li>

            <li class="nav-item">
                  <a class="nav-link" href="../mendel">Mendel</a>
                </li>             
            </ul>       
        </div>
      </div>
      <img class="d-none d-sm-none d-md-block" src="../img/camc-logo1.svg" alt="clickamc-logo" style="width:8%; margin:20px">
    </nav>
  </header>

  <!--welcome container-->
  <div class="container" style="font-family: 'Open Sans'">             
    <div id="column">

      <!--messages-->
        <?php echo $err ;?>      
        <?php echo $suc ;?>      
      <!--end messages-->

      <!--section welcome-->
      <h2 class="mt-3" style="text-align: center; font-family: 'Roboto Slab'">ClickAMC</h2>
      <!-- row welcome-->
      <div class="row justify-content-center">         
        <p>
          ClickAMC helps you get started with your own paper based survey project. The <a href="#App">ClickAMC App</a> generates the machine readable LaTeX questionnaire, the data dictionary, and the Stata DO file for labeling the answer dataset from one source file. All you need to do is to upload a TSV file that contains the metadata of the questionnaire. For more information about the metadata files' structure, take a look at the chapter "Work with Structured Metafiles" of the SurveyAMC Project <a href="https://survey.codes/pdf/surveyamc_manual.pdf" target="_blank">Manual</a>. Alternatively, you can download several <a href="#SSF">Sample Meta Files</a> further down on the page. For processing the filled answer sheets to a dataset (CSV), you can use the open source software <a href="https://www.auto-multiple-choice.net/" target="_blank">Auto-Multiple-Choice</a>.
        </p>        
      </div>
      <!--end row welcome-->

      <!--row demo-->
      <div class="row justify-content-center">         
        <a href="examples/demo.zip">
          <button type="button" class="btn btn-outline-primary btn-lg"> Download Demo Files</button>
        </a>       
      </div>

      <!--row gallery-->
      <div class="row justify-content-center" style="margin-top: 5%; text-align: center;"> 
        <div>              
          <h3 style="font-family: 'Roboto Slab'">Gallery</h3> 
          <p>
            Here are some questionnaire styles you can use with the <a href="#App">ClickAMC App</a> further down the page.        
          </p>  
        </div>

        <div id="carouselExampleIndicators" class="carousel slide formbox" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>              
            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
          </ol>
         
          <div class="carousel-inner">          
            <a href="../pdf/click-demo.pdf" target="_blank">
              <div class="carousel-item active">
                <img src="../img/potsdam1.svg" class="d-block w-50" alt="potsdam style 1">
              </div>
              <div class="carousel-item">
                <img src="../img/potsdam2.svg" class="d-block w-50" alt="potsdam style 2">
              </div>
              <div class="carousel-item">
                <img src="../img/berlin1.svg" class="d-block w-50" alt="berlin style 1">
              </div>
              <div class="carousel-item">
                <img src="../img/berlin2.svg" class="d-block w-50" alt="berlin style 2">
              </div>
              <div class="carousel-item">
                <img src="../img/cologne1.svg" class="d-block w-50" alt="cologne style 1">
              </div>                       
              <div class="carousel-item">
                <img src="../img/cologne2.svg" class="d-block w-50" alt="cologne style 2">
              </div>       
              <div class="carousel-item">
                <img src="../img/munich1.svg" class="d-block w-50" alt="munich style 1">
              </div>            
              <div class="carousel-item">
                 <img src="../img/munich2.svg" class="d-block w-50" alt="munich style 2">
              </div>            
              <div class="carousel-item">
                <img src="../img/hamburg1.svg" class="d-block w-50" alt="hamburg style 1">
              </div>                                    
              <div class="carousel-item">
                <img src="../img/hamburg2.svg" class="d-block w-50" alt="hamburg style 2">
              </div>
            </a> 
          </div>
          <!-- end carousel inner-->
         
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
        <!--end carousel-->             
      </div>
      <!--end row gallery-->  

      <!--form row-->
      <div class="row justify-content-center" id="App" style="margin-top: 5%;">  
        <!--section form-->
        <div class="col-12">
          <h3  style="text-align: center; font-family: 'Roboto Slab'">ClickAMC App</h3>   
        </div>
        <!--end section form-->

        <!--form col-->
        <div class="formbox col-sm col-lg-6" style="padding:5%">
          <!--form-->         
          <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data">
            <!-- form group browse-->     
            <div class="form-group row">                                                
              <div class="custom-file">                    
                <label style="overflow: hidden; font-size: 95%" id="csvfile_display" class="custom-file-label" for="datei">
                  Choose your meta file:</label>              
                <input type="file" onchange="getFileName()" id="csvfile" class="custom-file-input" name="datei" accept="text/csv/tsv">        
              </div>
            </div>
            <!--end form group browse-->

            <!-- form group layout-->
            <div class="form-group row">      
            <p style="text-align: left;"> Choose a questionnaire style:</p>                                   
              <select id="layout" name="layout" style="font-size: 90%" class="custom-select">                
                  <option selected value="1">Blank</option>
                  <option value="2">Potsdam</option>
                  <option value="3">Berlin</option>
                  <option value="4">Cologne</option>
                  <option value="5">Munich</option>
                  <option value="6">Hamburg</option>
              </select>
            </div> 
            <!--end form group layout-->

            <!--form group missing-->
            <div class="form-group row">   
              <input type="text" id="missing" name="missing" size="4" placeholder="99" class="col-sm-2 form-control">              
              <label for="missing" class="col-sm-9 col-form-label">
                Choose a missing value</label>
            </div>
            <!--end form group missing-->

            <!--form group converter-->
            <div class="form-group row"> 
              <div class="custom-control custom-checkbox">                
                <input type="checkbox" class="custom-control-input" id="chars" name="chars">
                <label class="custom-control-label" for="chars">Convert to TEX characters</label>                         
              </div>            
            </div>  
            <!--end form group converter-->          

            <!--form group send-->
            <div class="form-group row">              
              <button class="btn btn-outline-primary" type="submit" name="btn">Send</button>                      
            </div>
            <!--end form group send-->          
          
            <!--col form text-->      
            <div style="font-size: 70%; margin-top: 5%; text-align: justify;">
              Please take care that your meta file is tab separated, contains the column names: 
              <span style="color:  #AE8F15">
                id, class, type/scale, name, title
              </span>, 
              is not larger than 1 MB and does not contain any special characters except 
              <a href="chartable.html" target="_blank">these</a>.
            </div>
            <!--end col form text-->
          </form>
          <!--end form-->
           
          <!--include file name in browse-->
          <script>
            $(".custom-file-input").on("change", function() {
              var fileName = $(this).val().split("\\").pop();
              $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
            });
          </script>
          <!--end include file name in browse-->
        </div>
        <!--end form col-->
      </div>
      <!--end form row--> 
  
      <!--row samples-->
      <div class="row justify-content-center" style="margin-top: 5%; text-align: center;"> 
        <div class="col-12">      
          <h3 id="SSF" style="font-family: 'Roboto Slab'; ">Sample Meta Files</h3> 
        </div>
      
        <div class="col-lg-6">
          <p>  
            Here are some sample meta files from the SurveyAMC Tutorial available in english, german and french. Download one of them and then upload the file again using the form above.
          </p> 
        </div> 

        <div class="col-lg-12" style="text-align: center;">          
          <a href="examples/marx_en.tsv">
            <button type="button" class="btn btn-outline-primary btn-sm" style="width:100px;">english</button>
          </a>         
          <a href="examples/marx_de.tsv">
              <button type="button" class="btn btn-outline-primary btn-sm" style="width:100px;">german</button>
          </a>        
          <a href="examples/marx_fr.tsv">
            <button type="button" class="btn btn-outline-primary btn-sm" style="width:100px;">french</button>
          </a>
        </div>
      </div>
      <!--end row samples-->
      
      <!--row fork-->
      <div class="row justify-content-center" style="margin-top: 5%; text-align: center;"> 
        <div class="col-lg-12">
          <h3 class="mt-3" style="font-family: 'Roboto Slab';">Fork ClickAMC</h3>
         </div> 
          
         <div class="col-lg-6" style="text-align: justify;"> 	
            Use ClickAMC with R or on your server. The source code is available via the <a href="https://gitlab.com/CSaalbach/survey.codes" target="_blank">ClickAMC repository</a>. There you can download and modify the program.           
          
      	</div>
      </div>
      <!--end row fork-->

    </div>
    <!--end column-->


  </div>
  <!--end container-->    

      <!--footer--> 
      <div class="card text-center" style="margin-top: 10%;">      
        <div class="card-header">    
      </div>

      <div class="card-body">
        <h5 class="card-title">Contact</h5>
        <p style="text-align: center;">
          <a href="https://www.uni-potsdam.de/soziologie-methoden/" target="_blank" style="color: #484F56 !important;">Lehrstuhl Methoden der empirischen Sozialforschung</a>, <a href="https://www.uni-potsdam.de" target="_blank" style="color: #484F56 !important;">Universität Potsdam</a> 
        </p>        
      </div>
  
      <div class="card-footer text-muted">
        <a href="https://www.uni-potsdam.de/de/soziologie-methoden/impressum" target="_blank" style="color: #484F56 !important;">Impressum</a>
      </div>
      <!--end footer-->   
 </body>
</html>
