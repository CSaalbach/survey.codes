# ClickAMC stable-1.0                                                
# Claudia Saalbach, Johannes Schütt, 
# Chair of Empirical Methods, University of Potsdam 
# License: GNU General Public License v2.0 only     

#!/bin/bash
pdflatex gen_codebook.tex

rm *.log *.aux

mv gen_codebook.pdf output/codebook.pdf

mv questionnaire.pdf output/questionnaire.pdf

mv questionnaire.tex output/questionnaire.tex

cp quest_generator.R output/quest_generator.R

cp -r csv/ output/csv/

cp gen_codebook.tex output/gen_codebook.tex

cp content.csv output/content.csv

cp automultiplechoice.sty output/automultiplechoice.sty


