<!-- Datatblr stable-1.0                                                
Johannes Schütt, Claudia Saalbach
Chair of Empirical Methods, University of Potsdam 
License: GNU General Public License v2.0 only     
 -->

<?php

error_reporting(0); # DEBUGGING

define('ABSPATH', dirname(__FILE__).'/');
define('access', TRUE); # access to counter.php
include("gen_chars.php"); # include LaTeX chars

## check url
if ($_SERVER['HTTP_HOST'] != "survey.codes") {
        header("Location: https://survey.codes/datatblr");
}

## start function
function startsWith($haystack, $needle) {
     $length = strlen($needle);
     return (substr($haystack, 0, $length) === $needle);
}

function getName($n) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';

    for ($i = 0; $i < $n; $i++) {
        $index = rand(0, strlen($characters) - 1);
        $randomString .= $characters[$index];
    }

    return $randomString;
}
## end function

## preparation folder
$dir = getName(4); # create random 4-digit folder name

while (file_exists("tmp/" . $dir)) { # check if folder name is already taken
    $dir = getName(4);
}

$target_data_file = "tmp/" . $dir . "/csv/data.csv";
$target_meta_file = "tmp/" . $dir . "/csv/meta.csv";
$err = ""; # error message
$suc = ""; # success message
$title = "Datatblr"; # title of page
$upload = 1;

## start procedure

if(isset($_POST['btn'])) {

  $dataFileInfo = pathinfo($_FILES["datafile"]["name"]);
  $dataExt = strtolower($dataFileInfo["extension"]); # get data file extension

  $metaFileInfo = pathinfo($_FILES["metafile"]["name"]);
  $metaExt = strtolower($metaFileInfo["extension"]); # get data file extension


  if ($_FILES["datafile"]["name"] == "" || $_FILES["datafile"]["name"] == "") { # no file selected
    $upload = 0;
    $err = ' 
            <div class="row justify-content-center error">
              No file(s) selected.
            </div>  
           ';
  }
  else if (($dataExt != "csv" && $dataExt != "tsv") || ($metaExt != "csv" && $metaExt != "tsv")) { # check file type
    //else if ($_FILES["datei"]["type"] != "text/csv" && $_FILES["datei"]["type"] != "text/plain" && $_FILES["datei"]["type"] != "text/comma-separated-values" && $_FILES["datei"]["type"] != "application/vnd.ms-excel") { # File is no csv file
    $upload = 0;
    $err = ' 
            <div class="row justify-content-center error">
              The selected file is not a CSV/TSV file.
            </div>  ';
  }
  else if ($_FILES["datafile"]["size"] > 3000000 || $_FILES["metafile"]["size"] > 3000000) { # check file size
    $upload = 0;
    $err = ' 
            <div class="row justify-content-center error">
              One or both of the selected files are bigger then 3MB.
            </div>
           ';
  }

  if ($upload == 1) {

    $check = "id\tclass\ttype/scale\tname\ttext"; # column names of file
    $path = "tmp/" . $dir; # path of new folder
    $del = "rm -r " . $path; # preparing remove command for tmp dir in case of error

    mkdir($path); # create folder
    mkdir($path . "/csv");

    if (move_uploaded_file($_FILES["datafile"]["tmp_name"], $target_data_file) && move_uploaded_file($_FILES["metafile"]["tmp_name"], $target_meta_file)) {

      $h = fopen($target_meta_file, "r"); # open handle
      $firstline = fgets($h, 1000); # get column names of upload
      fclose($h); # close handle

      if (startsWith($firstline, $check)) { # strings match

        # write options.csv
        $h = fopen($path . "/csv/options.csv", 'w');
        fwrite($h, "option\tvalue\nmissings\t" . $_POST['missings'] . "\n");
        fclose($h);

        # copy shell program to dir
        copy("scripts/functions.R", $path . "/functions.R");
        copy("scripts/gen_f_m.R", $path . "/gen_f_m.R");
        copy("scripts/gen_l.R", $path . "/gen_l.R");
        copy("scripts/gen_n.R", $path . "/gen_n.R");
        copy("scripts/import.R", $path . "/import.R");
        copy("scripts/main.R", $path . "/main.R");
        copy("scripts/prep_output.R", $path . "/prep_output.R");
        copy("scripts/print_output.R", $path . "/print_output.R");

        # permissions
        chmod($path, 0777);

        chdir($path); # move to dir
        exec("Rscript main.R"); # run shell program ####

        if (!file_exists(ABSPATH. $path . "/report.pdf") OR !file_exists(ABSPATH . $path . "/report.tex")) {

          $err = ' 
            <div class="row justify-content-center error">
              An error ocurred. Please check your file for illegal characters.
            </div>
            ';
          chdir("../../"); # switch back to root
          exec($del); # removing tmp
        }
        else {

          $zipname = utf8_decode(basename($_FILES["datafile"]["name"], ".csv")); # name ZIP with upload file name
          $zipname = preg_replace("/[^a-zA-Z0-9]+/", "", $zipname); # rename if original name contains only alphanumerical characters

          if($zipname == "") { # if CSV name contais non-alphanumerical characters, rename ZIP to "output.zip"

            $zipname = "output";
          }

          $zipname = $zipname . ".zip";

          $zip = new ZipArchive;
          if ($zip->open($zipname, ZipArchive::CREATE) === TRUE) {

            # add files to ZIP
            $zip->addFile('report.pdf');
            $zip->addFile('report.tex');

            $zip->close(); # close ZIP
          }

          chdir("../../"); # switch back to root          

          $suc = '
          <div class="row justify-content-center">
            <div class="mx-auto" style="              
              width:600px;
              text-align: center; 
              margin-bottom: 5%;              
              color: #2C3744; 
              border: 1px dashed #28a745;
              border-radius: 5px;
              padding-top: 1.5%;
              padding-bottom: 1.5%;
              font-weight: 550;">
                Woohoo! Your tables are created. 
                <br>
                You can download them here:
                <br>                
                
                <a href="' . $path . '/' . $zipname . '"><button style="margin-top:2%;" type="button" class="btn btn-success">Download</button>
                </a>     
            </div>
           </div> 
        ';

        }

      }
      else {
        $err = ' 
            <div class="row justify-content-center error">
              Please check if the column names of your CSV file are correct, and if your CSV file is tab-delimited. If you are not sure about your CSV file structure, you can take a look at the sample files below or the documentation.
            </div>  
              ';
        exec($del);
      }
    }
      else { # move_uploaded_file error
        $err = "Something went wrong.";
        exec($del);
      }
    }
  }
?>

<!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="utf-8"/>
	<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
	<meta content="width=device-width, initial-scale=1" name="viewport"/>

  <title><?php echo $title; ?></title>

	<link href="../favicon.ico" rel="shortcut icon" type="image/x-icon"/>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<link href="../styles.css" rel="stylesheet"/>

	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,500|Roboto:400,500&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400&display=swap" rel="stylesheet">
    <style type="text/css">
     .row {
        margin-left: -5px;
        margin-right: -5px;        
      }
      .row > form {
        border: 1px solid #F3C71E;
        border-radius: 5px;
        margin-top: 2.5%;
        margin-bottom: 2.5%;
        padding:5%; 
      }
        
    .formbox {
        border: 1px solid #F3C71E;
        border-radius: 5px;
      }  
    .btn-primary {
        color: #fff;
        background-color: #007bff;
        border-color: #007bff;
      }

      .btn-primary:hover {
        color: #fff;
        background-color: #0069d9;
        border-color: #0062cc;
      }

      .btn-primary:focus, .btn-primary.focus {
        color: #fff;
        background-color: #0069d9;
        border-color: #0062cc;
        box-shadow: 0 0 0 0.2rem rgba(38, 143, 255, 0.5);
      }

      .error{
        text-align: center; 
        margin-top: 2.5%;
        margin-bottom:5%;        
        color: #CD4F39; 
        font-size: 80%;
        border: 1px dashed #CD4F39;
        border-radius: 5px;
        padding: 3%;        
      }      
  </style>
</head>

<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-light bg-light">
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
			</button>

		  	<a class="navbar-brand" href="https://survey.codes/">survey.codes</a>

		  	<div class="collapse navbar-collapse" id="navbarTogglerDemo03">

				<div class="container">

			    	<ul class="navbar-nav mr-auto mt-2 mt-lg-0 justify-content-center">
			      		<li class="nav-item">
			        		<a class="nav-link" href="../surveyamc">SurveyAMC</a>
			      		</li>

			      		<li class="nav-item">
			        		<a class="nav-link" href="../click">ClickAMC</a>
			      		</li>

			      		<li class="nav-item active">
			        		<a class="nav-link" href="../datatblr">Datatblr</a>
			      		</li>

						<li class="nav-item">
			        		<a class="nav-link" href="../mendel">Mendel</a>
			      		</li>
			    	</ul>
				</div>
			</div>
			<img class="d-none d-sm-none d-md-block" src="../img/datatblr-logo1.svg" alt="datatblr-logo" style="width:8%; margin:20px">
		</nav>
	</header>

	<div class="container">
	  	<div id="column">

      <!--messages-->
        <?php echo $err ;?>      
        <?php echo $suc ;?>      
      <!--end messages--> 

		    <h2 class="mt-3" style="text-align: center">Datatblr</h2>
				<p>
				The Datatblr program generates a table volume that contains the univariate description of all metric and categorical variables of a data set. Datatblr delivers the table volume as LaTeX and PDF file. All you have to do is upload a CSV file with your survey response data and a CSV file with the meta information of your questionnaire. Datatblr is suitable for generating table volumes that typically appear in the appendix of research reports.
				</p>
		</div>
	</div>

  <div class="row justify-content-center" style="margin-top:3%; font-family: Open Sans">    
      <a href="https://survey.codes/datatblr/examples/kksoep.zip"><button type="button" class="btn btn-outline-primary btn-lg">Download Demo Files</button></a>
    
  </div>

    <div class="container">  				  	
	  	<div id="column">
        <div class="row justify-content-center">         

          <div class="col-sm col-lg-6">
    		    <h3 class="mt-3" style="text-align: center;">Gallery</h2>
          </div>

  			  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  			  	<ol class="carousel-indicators">
  			    	<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>

  			    	<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
  			    	
  			    	<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  			  	</ol>
  			 
  			  	<div class="carousel-inner" style=" background-color: #f8f9fa !important; border: 1px solid #F3C71E; border-radius: 5px;">
  			    
  			    	<div class="carousel-item active">
  				      	<a href="../pdf/d_kksoep.pdf"><img src="../img/d_kksoep_1.jpg" class="d-block w-50" alt="questionnaire example 1"></a>
  				    </div>

  				    <div class="carousel-item">
  				    	<a href="../pdf/d_kksoep.pdf"><img src="../img/d_kksoep_2.jpg" class="d-block w-50" alt="questionnaire example 2"></a>
  				    </div>

  				    <div class="carousel-item">
  				    	<a href="../pdf/d_kksoep.pdf"><img src="../img/d_kksoep_3.jpg" class="d-block w-50" alt="questionnaire example 3"></a>
  				    </div>			    
  			    
  				    <div class="carousel-item">
  				    	<a href="../pdf/d_kksoep.pdf"><img src="../img/d_kksoep_8.jpg" class="d-block w-50" alt="questionnaire example 4"></a>
  				    </div>

  				    <div class="carousel-item">
  				    	<a href="../pdf/d_kksoep.pdf"><img src="../img/d_kksoep_9.jpg" class="d-block w-50" alt="questionnaire example 5"></a>
  				    </div>

  			  	</div>          
  			 
  				  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
  				    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
  				    <span class="sr-only">Previous</span>
  				  </a>
			  
  			  	<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
  			    	<span class="carousel-control-next-icon" aria-hidden="true"></span>
  			    	<span class="sr-only">Next</span>
  			  	</a>
          </div>
			  </div> 
    	</div>
	  </div>

	<div class="container">
	  <div id="column">
      <!--form row-->
      <div class="row justify-content-center" id="App" style="margin-top: 5%;">  

        <!--section form-->
        <div class="col-12">
	         <h3 class="mt-3" style="text-align: center;">Upload your Files</h3>
        </div>

        <!--form col-->
        <div class="formbox col-sm col-lg-6" style="padding:5%">

  	      <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" enctype="multipart/form-data" style="font-family: 'Open Sans';">
  	          <!--browse data file-->
              <div class="form-group row" >
  	            <div class="custom-file">
  	              <input type="file" onchange="getDataFileName()" id="datafile" class="custom-file-input" name="datafile" accept="text/csv, text/tab-separated-values">
  	              <label style="overflow: hidden;" id="datafile_display" class="custom-file-label" for="name">Choose data file</label>
  	            </div>
  	          </div>

  	          <!--browse meta file-->
              <div class="form-group row" >            
  	            <div class="custom-file">
  	              <input type="file" onchange="getMetaFileName()" id="metafile" class="custom-file-input" name="metafile" accept="text/csv, text/tab-separated-values">
  	              <label style="overflow: hidden;" id="metafile_display" class="custom-file-label" for="name">Choose meta file</label>
  	            </div>
  	          </div>

              <!--missing options-->
              <div class="form-group row" >	            
  	            <!--option 1-->
  	            <div class="form-check" style="margin-right: 40px;">
  	              <input type="radio" value="1" class="form-check-input" id="option1" name="missings" checked>
  	              <label class="form-check-label" for="option1">Summarize missings</label>
  	            </div>

  	            <!--option 2-->
  	            <div class="form-check">
  	              <input type="radio" value="2" class="form-check-input" id="option2" name="missings">
  	              <label class="form-check-label" for="option2">Ignore missings</label>
  	            </div>
  	          </div>

              <!--send-->
              <div class="form-group row" >             	          
  	            <div class="input-group-append">
  	              <button class="btn btn-outline-primary" type="submit" name="btn">Send</button>
  	            </div>
  	          </div>
  	      </form>                
        </div>
        <!--form col-->            
      </div>
      <!--form row-->
    </div>
  </div>

    <div class="container">
      <div id="column">
          <div class="row justify-content-center">
            <div class="col-sm col-lg-6">
              <h3 class="mt-3" style="text-align: center;">Sample Files</h3>
            	<p>
        			Try Datatblr with our sample files. The data file contains the questionnaires' response data, and the meta file includes the questionnaires' meta information. Download them and then upload them again using the form above.
        		  </p> 
             </div> 
              
            <div class="col-12" style="text-align: center; font-family: Open Sans;">                  		  
              <a href="examples/data.csv">
                <button type="button" class="btn btn-outline-primary btn-sm" style="width:100px;">data.csv</button>
              </a>
              <a href="examples/meta.tsv">
                <button type="button" class="btn btn-outline-primary btn-sm" style="width:100px;">meta.tsv</button>
              </a>        
            </div>      

            <div class="col-sm col-lg-6" style="margin-top: 2%">
              <p>
    			    You may also take a look at the meta file to get to know its' structure. Further information about how to create a meta file you will find in the SurveyAMC <a href="https://survey.codes/pdf/surveyamc_manual.pdf" target="_blank">Manual</a>.
    		      </p>       
            </div>
              
          </div>
        </div>
       </div> 

    <div class="container">
      <div id="column">
        <div class="row justify-content-center">
          <div class="col-sm col-lg-6">
            <h3 class="mt-3" style="text-align: center;">Desktop Version</h3>

            <p>
  		      Datatblr is also available as a desktop version if you want to use it locally on your device. The <a href="https://github.com/johschuett/Datatblr" target="_blank">Datatblr repository</a> provides further information about installation and usage.	</p>
          </div>
        </div>    
      </div>  
    </div>

    <div class="container">
      <div id="column">    
        <div class="row justify-content-center">
          <div class="col-sm col-lg-6">
            <h3 class="mt-3" style="text-align: center;">Fork Datatblr</h3>
            <p>
        		The source code is freely available via the <a href="https://gitlab.com/CSaalbach/survey.codes" target="_blank">Datatblr repository</a>. There you can download and modify the program.
            </p> 
	        </div>	
        </div>  
      </div>  
    </div>  

  </div>
</div>

  <div class="card text-center" style="margin-top: 50px;">
      
      <div class="card-header">    
      </div>

      <div class="card-body">
        <h5 class="card-title">Contact</h5>
        <p style="text-align: center;">
	  <a href="https://www.uni-potsdam.de/soziologie-methoden/" target="_blank" style="color: #484F56 !important;">Lehrstuhl Methoden der empirischen Sozialforschung</a>,
	  <a href="https://www.uni-potsdam.de" target="_blank" style="color: #484F56 !important;">University of Potsdam</a> 
        </p>
        
      </div>
  
      <div class="card-footer text-muted">
        <a href="https://www.uni-potsdam.de/de/soziologie-methoden/impressum" target="_blank" style="color: #484F56 !important;">Impressum</a>
      </div>
  </div>



    <!-- jQuery (Bootstrap JS plugins depend on it) -->
    <script src="jsn/jquery-3.1.0.min.js"></script>
    <script src="jsn/bootstrap.min.js"></script>
    <script src="jsn/script.js"></script>

    <script>
  		function getDataFileName() {
  			var fullPath = document.getElementById('datafile').value;
  			if (fullPath) {
   			   var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
     			   var filename = fullPath.substring(startIndex);
     		           if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
         			  	 filename = filename.substring(1);
      			   }
      			   document.getElementById('datafile_display').innerHTML = filename;
  			}
  		}

      function getMetaFileName() {
  			var fullPath = document.getElementById('metafile').value;
  			if (fullPath) {
   			   var startIndex = (fullPath.indexOf('\\') >= 0 ? fullPath.lastIndexOf('\\') : fullPath.lastIndexOf('/'));
     			   var filename = fullPath.substring(startIndex);
     		           if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
         			  	 filename = filename.substring(1);
      			   }
      			   document.getElementById('metafile_display').innerHTML = filename;
  			}
  		}
  	</script>
 </body>
</html>
