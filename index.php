<?php
if ($_SERVER['HTTP_HOST'] != "survey.codes") {
	header("Location: https://survey.codes/");
}
?>

<!DOCTYPE html>
<html lang="de">
<head>
	<meta charset="utf-8"/>
	<meta content="IE=edge" http-equiv="X-UA-Compatible"/>
	<meta content="width=device-width, initial-scale=1" name="viewport"/>

	<title>survey.codes</title>
	
	<link href="favicon.ico" rel="shortcut icon" type="image/x-icon"/>
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<link href="styles.css" rel="stylesheet"/>
	  
	<link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,500|Roboto:400,500&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400&display=swap" rel="stylesheet"> 

<style>
	.hover img:hover {	
		opacity: .7;
	}
</style>
</head>

<body>
	<header>
		<nav class="navbar navbar-expand-lg navbar-light bg-light">			
		  
		  	<a class="navbar-brand mx-auto" href="https://survey.codes/">survey.codes</a>	  	
			
		</nav>
	</header>


	<div class="container d-flex justify-content-center align-items-center" style="margin-top: 5%;">
	  <div class="row">

		    <div class="col-12 col-md-3 col-lg-3 justify-content-center" style="margin-top:10%;">
		    	<div class="in text-center">
		    		SurveyAMC <br>
		    		<a href="surveyamc" target="_blank">
		    			<div class="hover">
		    				<img src="img/samc-logo7.svg" alt="surveyamc-logo" style="width:100px; margin-top: 5px;">
		    			</div>
		    		</a>		    
		    	</div>
		    </div>

		    <div class="col-12 col-md-3 col-lg-3 justify-content-center"  style="margin-top:10%;">
		    	<div class="in text-center">
		    		ClickAMC <br>
		    		<a href="click" target="_blank">
		    			<div class="hover">
		    				<img src="img/camc-logo1.svg" alt="clickamc-logo" style="width:100px; margin-top: 5px;">
		    			</div>	
		    		</a>		    
		    	</div>
		    </div>

		    <div class="col-12 col-md-3 col-lg-3 justify-content-center"  style="margin-top:10%;">
		    	<div class="in text-center">
		    		Datatblr<br>
		    		<a href="datatblr" target="_blank">
		    			<div class="hover">
		    				<img src="img/datatblr-logo1.svg" alt="datatblr-logo" style="width:100px;margin-top: 20px;">
		    			</div>	
		    		</a>		    
		    	</div>
		    </div>

		    <div class="col-12 col-md-3 col-lg-3 justify-content-center"  style="margin-top:10%;">
		    	<div class="in text-center">
		    		Mendel<br>
		    		<a href="mendel" target="_blank">
		    			<div class="hover">
	    					<img src="img/mendel-logo6.svg" alt="mendel-logo" style="width:100px;margin-top: 17px;margin-left: 3px;">		    				
	    				</div>	
		    		</a>		    
		    	</div>
		    </div>
		  </div>
		</div>    

		<div class="container d-flex justify-content-center align-items-center" style="margin-top: 2%; font-size: 80%">
		  <div class="row">
			<div class="col-2 justify-content-center">
			</div>	

			<div class="col-8 justify-content-center">
			 	<div class="in text-center">	
			  		<p> 
			  			The survey.codes project focuses on how metadata can be reused to produce different survey products during the survey life cycle. For example, to create measurement instruments for different modes (paper & web), for documentation or analysis of the response dataset. The metadata structure used in the survey.codes project is based on the metadata structure of the online survey software <a href="https://www.limesurvey.org" target="_blank">Limesurvey</a>. Survey.codes can be used as a web application as well as a local console application. All scripts are freely accessible via a <a href="https://gitlab.com/CSaalbach/survey.codes" target="_blank">Git Repository</a> and can be modified for other metadata structures. 
			  		</p>	
			  		<p>
						The survey.codes project (<a href="https://doi.org/10.5281/zenodo.4911340"><img src="https://zenodo.org/badge/DOI/10.5281/zenodo.4911340.svg" alt="DOI"></a>) is developed at the Chair for Methods of Empirical Social Research at the University of Potsdam by <a href="https://www.diw.de/de/diw_01.c.815495.de/personen/saalbach__claudia.html" target="_blank">Claudia Saalbach</a> (project management) and <a href="https://www.uni-potsdam.de/de/soziologie-methoden/team/hilfskraefte" target="_blank">Johannes Schütt</a> (research assistant). 
					</p>
				</div>
			</div>	

			<div class="col-2 justify-content-center">
			</div>
		  </div>	   
		</div>
	

	<div class="card text-center" style="margin-top: 10%;">
  		
  		<div class="card-header">    
  		</div>

  		<div class="card-body">
    		<h5 class="card-title">Contact</h5>
    		<p style="text-align: center;">
    			<a href="https://www.uni-potsdam.de/soziologie-methoden/" target="_blank" style="color: #484F56 !important;">Lehrstuhl Methoden der empirischen Sozialforschung</a>, Universität Potsdam	
    		</p>
    		
  		</div>
  
  		<div class="card-footer text-muted">
    		<a href="https://www.uni-potsdam.de/de/soziologie-methoden/impressum" target="_blank" style="color: #484F56 !important;">Impressum</a>
  		</div>
	</div>





    <!-- jQuery (Bootstrap JS plugins depend on it) -->
    <script src="jsn/jquery-3.1.0.min.js"></script>
    <script src="jsn/bootstrap.min.js"></script>
    <script src="jsn/script.js"></script>
 </body>
</html>






         





